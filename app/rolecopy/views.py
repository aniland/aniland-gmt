# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.rolecopy import models


@login_required
@permission_required('gpermission.rolecopy_read')
def index(request):
    data_list = list(models.Rolecopy.objects.all())
    ctx = {'dataList': {}}

    if data_list:
        ctx['dataList'] = data_list
    else:
        pass

    return layout.render_with_layout(request,
                                     'rolecopy',
                                     'rolecopy_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.rolecopy_write')
@lock_required('rolecopy')
def addone(request):
    models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])

@login_required
@permission_required('gpermission.rolecopy_write')
@lock_required('rolecopy')
def get_role_data(request):
    if request.method == 'POST':
        print(request.POST)
        fpId = int(request.POST['fpid'])
        roleId = int(request.POST['roleid'])
        serverId = int(request.POST['serverid'])
        ret = models.pull_role_data_from_game_server(serverId,fpId,roleId)

        # json

        # print(ret['full_data'])

        models.add_model(serverId,fpId,roleId,ret['full_data'])
        return redirect("rolecopy_index")

@login_required
@permission_required('gpermission.rolecopy_write')
@lock_required('rolecopy')
def import_role_data(request):
    if request.method == 'POST':
        print(request.POST)
        id = int(request.POST['id'])
        fpId = int(request.POST['fpid'])
        roleId = int(request.POST['roleid'])
        serverId = int(request.POST['serverid'])

        data = models.get_model(id)
        ret = models.push_role_data_to_game_server(serverId, fpId, roleId,data.fullData)
        print(ret)
        return redirect("rolecopy_index")


@login_required
@permission_required('gpermission.rolecopy_write')
@lock_required('rolecopy')
def delete(request):
    if request.method == 'POST':
        print(request.POST)
        models.delete_model(request.POST['id'])
        return redirect("rolecopy_index")