# coding:utf-8

import json
from django.http import HttpResponseNotAllowed
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from pypuzlib import httprpc as pyrpc
from library import layout
from library.lock import has_lock


@login_required
def rpc(request):
    if not request.is_ajax():
        messages.error(request, 'rpc should be ajax request')
        return HttpResponseNotAllowed

    payload = json.loads(request.body)
    if not payload:
        return layout.response_json({}, code=-1, message='payload is missing')

    mname = payload.get('module', '')
    if not mname:
        return layout.response_json({}, code=-1, message='module is missing')

    module = layout.get_module(request, mname)
    if not module:
        return layout.response_json({}, code=-1, message='unknown module')

    perm = module['permission']
    if not request.user.has_perm(perm):
        return layout.response_json({}, code=-1, message='no permission')

    lock = module['lock']
    if not has_lock(request.user.username, lock):
        return layout.response_json({}, code=-1, message='module locked')

    cname = payload.get('client', '')
    if not cname:
        return layout.response_json({}, code=-1, message='client is missing')

    client = pyrpc.clients.get(cname, {})
    if not client:
        return layout.response_json({}, code=-1, message='unknown client')

    id = payload.get('id', '')
    if not id:
        return layout.response_json({}, code=-1, message='id is missing')

    data = payload.get('data', {})
    try:
        code, message, payload = client.request(id, data)
    except Exception as ex:
        return layout.response_json({}, code=-1, message=str(ex))
    else:
        return layout.response_json(payload, code, message)
