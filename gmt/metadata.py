# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.metadata import METADATA as LIBMETADATA


METADATA = {
    **LIBMETADATA,
    'sandbox': {
        'models': [
            'app.sandbox.models.Sandbox',
        ],
        'permission': 'gpermission.sandbox_write',
        'lock': 'sandbox',
        'title': _('Sandbox'),
        'process': 'app.sandbox.models.metadata_process',
    },
    
    'qualitySetting': {
        'models': [
            'app.qualitySetting.models.Qualitysetting',
            'app.qualitySetting.models.QualitysettingConf',
        ],
        'permission': 'gpermission.qualitySetting_write',
        'lock': 'qualitySetting',
        'title': _('Qualitysetting'),
        'process': 'app.qualitySetting.models.metadata_process',
    },

    'whitelistfpid': {
        'models': [
            'app.whitelistfpid.models.Whitelistfpid',
        ],
        'permission': 'gpermission.whitelistfpid_write',
        'lock': 'whitelistfpid',
        'title': _('Whitelistfpid'),
        'process': 'app.whitelistfpid.models.metadata_process',
    },

    'whitelistip': {
        'models': [
            'app.whitelistip.models.Whitelistip',
        ],
        'permission': 'gpermission.whitelistip_write',
        'lock': 'whitelistip',
        'title': _('Whitelistip'),
        'process': 'app.whitelistip.models.metadata_process',
    },

    'sealfpid': {
        'models': [
            'app.sealfpid.models.Sealfpid',
        ],
        'permission': 'gpermission.sealfpid_write',
        'lock': 'sealfpid',
        'title': _('Sealfpid'),
        'process': 'app.sealfpid.models.metadata_process',
    },

    'serverstatus': {
        'models': [
            'app.serverstatus.models.Serverstatus',
        ],
        'permission': 'gpermission.serverstatus_write',
        'lock': 'serverstatus',
        'title': _('Serverstatus'),
        'process': 'app.serverstatus.models.metadata_process',
    },

    'globalSvrAddr': {
        'models': [
            'app.globalSvrAddr.models.Globalsvraddr',
        ],
        'permission': 'gpermission.globalSvrAddr_write',
        'lock': 'globalSvrAddr',
        'title': _('Globalsvraddr'),
        'process': 'app.globalSvrAddr.models.metadata_process',
    },

    'globalRoutingRules': {
        'models': [
            'app.globalRoutingRules.models.Globalroutingrules',
        ],
        'permission': 'gpermission.globalRoutingRules_write',
        'lock': 'globalRoutingRules',
        'title': _('Globalroutingrules'),
        'process': 'app.globalRoutingRules.models.metadata_process',
    },

    'kickplayer': {
        'models': [
            'app.kickplayer.models.Kickplayer',
        ],
        'permission': 'gpermission.kickplayer_write',
        'lock': 'kickplayer',
        'title': _('Kickplayer'),
        'process': 'app.kickplayer.models.metadata_process',
    },

    'deleteuser': {
        'models': [
            'app.deleteuser.models.Deleteuser',
        ],
        'permission': 'gpermission.deleteuser_write',
        'lock': 'deleteuser',
        'title': _('Deleteuser'),
        'process': 'app.deleteuser.models.metadata_process',
    },

    'rolecopy': {
        'models': [
            'app.rolecopy.models.Rolecopy',
        ],
        'permission': 'gpermission.rolecopy_write',
        'lock': 'rolecopy',
        'title': _('Rolecopy'),
        'process': 'app.rolecopy.models.metadata_process',
    },

    'GmtTest': {
        'models': [
            'app.GmtTest.models.Gmttest',
        ],
        'permission': 'gpermission.GmtTest_write',
        'lock': 'GmtTest',
        'title': _('Gmttest'),
        'process': 'app.GmtTest.models.metadata_process',
    },
}
