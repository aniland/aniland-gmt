# coding:utf-8

import json
from django.core import serializers
from django.db import models

from django.conf import settings
from pypuzlib import httprpc as rpc

# todo: 迁移到urls.py
if settings.GMT_GLOBAL_URL:
    if settings.GMT_GLOBAL_NAME not in rpc.clients:
        rpc.add_client(settings.GMT_GLOBAL_NAME,
                       settings.GMT_GLOBAL_SECRET,
                       settings.GMT_GLOBAL_URL.rstrip('/') + '/gm')

class Globalsvraddr(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Globalsvraddr.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Globalsvraddr(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Globalsvraddr.objects.all()))])


def get_all_svr_addr():
    c, m, p = rpc.clients[settings.GMT_GLOBAL_NAME].request('get-svr-addr', [])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def del_svr_addr(gameId, gameEnvironment):
    c, m, p = rpc.clients[settings.GMT_GLOBAL_NAME].request('del-svr-addr',
                                                            {'game_id': gameId, 'environment': gameEnvironment})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def add_svr_addr(gameId, gameEnvironment, serverAddr):
    c, m, p = rpc.clients[settings.GMT_GLOBAL_NAME].request('add-svr-addr',
                                                            {'game_id': gameId, 'server_addr': serverAddr,
                                                             'environment':gameEnvironment})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}
