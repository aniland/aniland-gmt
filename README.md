# Antia GMT Repositry

## Prerequisites

* Pipenv latest version
* MySQL or MarixoaDB >= 5.6

## Bootstrap

* Prepare and make git work with bitbucket in SSH mode
  
  ```shell
  git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
  ```

* Create mysql user with name 'root' and leave it's password empty
  
* Init mysql database

  ```shell
  mysql -uroot < sql/init.sql
  ```

* Install python dependencies

  ```shell
  pipenv install
  ```

* Enter pipenv environment
  
  ```shell
  pipenv shell
  ```

* Prepare static resources for django
  
  ```shell
  python manage.py collectstatic
  ```

* Create migration files for mysql
  
  ```shell
  python manage.py makemigrations
  ```

* Create tables and init table's data for mysql
  
  ```shell
  python manage.py migrate
  ```

* Create super user
  
  ```shell
  python manage.py createsuperuser
  ```

* Startup the server
  
  ```shell
  python manage.py runserver localhost:8080
  ```

* Open you browser and type the following url to play with it

  ```url
  http://localhost:8080
  ```

### How To Test

### Add Module
* ./manage.py addmodule {module_name}
* ./manage.py makemigrations {module_name}
* ./manage.py migrate
* ./manage.py runserver {address}


### uwsgi.ini
```shell
[uwsgi]
    socket = 0.0.0.0:8110
    #stats-server = :8113
    #virtualenv = /data/.payment/
    #chdir = /data/project/PuzzlePayment/
    #module = payments.wsgi:application
    master = true
    workers = 10
    pidfile = /var/run/uwsgi.pid
    max-requests = 10000
    logto = /data/log/uwsgi.log
    enable-threads = true
    single-interpreter = true
    event = epoll
    listen = 128
    lazy = true
    harakiri = 120
    harakiri-verbose = true
    disable-logging = true
    buffer-size = 65535
```


### nginx.config
```shell
server {
    listen      80;
    charset     utf-8;
    server_name antia-prod-gmt.puzzleplusgames.net;

    root        /data/project/gmt/;

    access_log  /data/log/nginx/gmt.log;
    error_log   /data/log/nginx/gmt.log;

    index index.php index.html index.htm;


    location /static/ {
        alias /data/project/gmt/public/;
    }




    #deny all;
    ## test
#    try_files $uri $uri/ @rewrite;
#    location @rewrite {
#        rewrite ^(.*)$ /index.php?_url=$1;
#    }



    location / {
            include uwsgi_params;
            uwsgi_pass 127.0.0.1:8110;

            uwsgi_param UWSGI_PYHOME $document_root;
            uwsgi_param UWSGI_CHDIR $document_root;
            uwsgi_param UWSGI_SCRIPT gmt_uwsgi;
        }

    location /nginx_status {
            stub_status on;
            access_log  off;
        }

}
```
