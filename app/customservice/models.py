# coding: utf-8

import json
from django.conf import settings
from pypuzlib import httprpc as rpc

# todo: 迁移到urls.py
if settings.GMT_APP_URL:
    if settings.GMT_APP_NAME not in rpc.clients:
        rpc.add_client(settings.GMT_APP_NAME,
                       settings.GMT_APP_SECRET,
                       settings.GMT_APP_URL.rstrip('/') + '/gm')


def get_plog_settings():
    # pull from remote storage
    # _init_rpc()
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('getPlogSetting', [])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def update_plog_setting(info):
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('updatePlogSetting', info)
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))

    return p if p else {}


def plog_delete(tag):
    return rpc.clients[settings.GMT_APP_NAME].request('deletePlogSetting', tag)
