# coding:utf-8

from library.config.crud import CRUD as LIBCRUD

CRUD = {
    **LIBCRUD,
    'sandbox': {
        'model': 'app.sandbox.models.Sandbox',
        'permission': 'gpermission.sandbox_write',
        'lock': 'sandbox',
    },

    'mail': {
        'model': 'app.mail.models.Mail',
        'permission': 'gpermission.mail_write',
        'lock': 'mail',
    },

    'qualitySetting': {
        'model': 'app.qualitySetting.models.Qualitysetting',
        'permission': 'gpermission.qualitySetting_write',
        'lock': 'qualitySetting',
    },

    'whitelistip': {
        'model': 'app.whitelistip.models.Whitelistip',
        'permission': 'gpermission.whitelistip_write',
        'lock': 'whitelistip',
    },

    'whitelistfpid': {
        'model': 'app.whitelistfpid.models.Whitelistfpid',
        'permission': 'gpermission.whitelistfpid_write',
        'lock': 'whitelistfpid',
    },

    'sealfpid': {
        'model': 'app.sealfpid.models.Sealfpid',
        'permission': 'gpermission.sealfpid_write',
        'lock': 'sealfpid',
    },

    'qualitySettingConf': {
        'model': 'app.qualitySetting.models.QualitysettingConf',
        'permission': 'gpermission.qualitySetting_write',
        'lock': 'qualitySetting',
    },

    'serverstatus': {
        'model': 'app.serverstatus.models.Serverstatus',
        'permission': 'gpermission.serverstatus_write',
        'lock': 'serverstatus',
    },

    'globalSvrAddr': {
        'model': 'app.globalSvrAddr.models.Globalsvraddr',
        'permission': 'gpermission.globalSvrAddr_write',
        'lock': 'globalSvrAddr',
    },

    'globalRoutingRules': {
        'model': 'app.globalRoutingRules.models.Globalroutingrules',
        'permission': 'gpermission.globalRoutingRules_write',
        'lock': 'globalRoutingRules',
    },

    'kickplayer': {
        'model': 'app.kickplayer.models.Kickplayer',
        'permission': 'gpermission.kickplayer_write',
        'lock': 'kickplayer',
    },

    'deleteuser': {
        'model': 'app.deleteuser.models.Deleteuser',
        'permission': 'gpermission.deleteuser_write',
        'lock': 'deleteuser',
    },

    'rolecopy': {
        'model': 'app.rolecopy.models.Rolecopy',
        'permission': 'gpermission.rolecopy_write',
        'lock': 'rolecopy',
    },

    'GmtTest': {
        'model': 'app.GmtTest.models.Gmttest',
        'permission': 'gpermission.GmtTest_write',
        'lock': 'GmtTest',
    },
}
