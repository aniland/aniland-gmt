# Generated by Django 2.1.10 on 2019-11-04 15:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gpermission', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gpermission',
            options={'managed': False, 'permissions': [('lock_read', 'Lock Read'), ('lock_write', 'Lock Write'), ('account_user_read', 'Account User Read'), ('account_user_write', 'Account User Write'), ('account_group_read', 'Account Group Read'), ('account_group_write', 'Account Group Write'), ('metadata_management_read', 'Metadata Management Read'), ('metadata_management_write', 'Metadata Management Write'), ('sandbox_read', 'Sandbox Read'), ('sandbox_write', 'Sandbox Write'), ('gds_read', 'GDS Read'), ('gds_write', 'GDS Write'), ('event_read', 'Event Read'), ('event_write', 'Event Write'), ('userinfo_read', 'User Info Read'), ('userinfo_write', 'User Info Write'), ('userprobe_read', 'User Probe Read'), ('userprobe_write', 'User Probe Write')]},
        ),
    ]
