# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Gmttest(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Gmttest.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Gmttest(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Gmttest.objects.all()))])
