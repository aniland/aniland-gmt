# coding: utf-8

from django.conf import settings
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from django.utils.module_loading import import_string

PERMISSIONS = import_string(settings.PERMISSION_CONFIG) if \
    hasattr(settings, 'PERMISSION_CONFIG') and settings.PERMISSION_CONFIG \
    else {}


class GPermission(models.Model):
    class Meta:
        managed = False
        permissions = PERMISSIONS


def all_permissions():
    try:
        ctype = ContentType.objects.get(app_label='gpermission',
                                        model='gpermission')
        perms = list(Permission.objects.filter(content_type_id=ctype.id))
        all_perms = [p[0] for p in PERMISSIONS]
        permissions = [p for p in perms if p.codename in all_perms]
        return permissions
    except ContentType.DoesNotExist:
        return []
