# coding:utf-8

import json
from django.core import serializers
from django.db import models
from django.utils import timezone

from enum import Enum


# 服务器操作状态
class EServerStatus(Enum):
    ESS_Wait_Opened = 1
    ESS_Wait_Closed = 2
    ESS_Opened = 3
    ESS_Closed = 4


def getServerStatusString(status):
    if status == EServerStatus.ESS_Wait_Opened.value:
        return "wait open"
    elif status == EServerStatus.ESS_Wait_Closed.value:
        return "wait close"
    elif status == EServerStatus.ESS_Opened.value:
        return "opened"
    elif status == EServerStatus.ESS_Closed:
        return "closed"
    else:
        return "None"


class Serverstatus(models.Model):
    open_or_close = models.IntegerField(default=0)
    effective_time = models.CharField(max_length=128)


def get_model(id):
    try:
        return Serverstatus.objects.get(id=id)
    except Exception:
        return {}


def metadata_process():
    data_list = list(Serverstatus.objects.all().order_by('-id'))
    if data_list:
        v = data_list[0]
        return json.dumps({"id": v.id,
                           "open_or_close": v.open_or_close,
                           "effective_time": timeToTimestamp(v.effective_time),
                           "str_effective_time": v.effective_time,
                           "op desc":getServerStatusString(v.open_or_close)
                           })
    else:
        return json.dumps({"id": 0, "open_or_close": 1, "effective_time": 0, "str_effective_time": ""})
    # return json.dumps({"data": [p.get('fields', {}) for p in json.loads(
    #   serializers.serialize('json',
    #                          Serverstatus.objects.all()))]})


def timestampToTime(timestamp):
    from datetime import datetime

    timeArray = datetime.utcfromtimestamp(timestamp)
    result = timeArray.strftime("%Y-%m-%d %H:%M:%S")
    return result


def timeToTimestamp(str_time):
    from datetime import datetime
    import time

    timeArray = datetime.strptime(str_time, '%Y-%m-%d %H:%M:%S').utctimetuple()

    # make time
    timestamp = int(time.mktime(timeArray)) * 1000
    return timestamp
