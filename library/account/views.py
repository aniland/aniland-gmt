# coding: utf-8


import json
from django.contrib import messages
from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import (
    User as UserModel, Group as GroupModel, Permission as PermissionModel
)

from django.http.response import HttpResponseNotAllowed
from django.utils.translation import gettext as _
from django.shortcuts import render, redirect
from library.gpermission.models import all_permissions
from library.layout import render_with_layout, response_json
from library.lock import lock_required


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request,
                             _('Your password was successfully updated!'))
            return redirect('home')
        else:
            return render(request, 'change_password.html', {})
    elif request.method == 'GET':
        form = PasswordChangeForm(request.user)
        return render(request, 'change_password.html', {})
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])


@login_required
@permission_required('gpermission.account_user_read')
def user_index(request):
    users = UserModel.objects.filter(is_superuser=False, is_active=True)
    ctx = {'users': list(users) if users else []}
    if ctx['users']:
        for u in ctx['users']:
            u.grps = [g.name for g in u.groups.all()]
    return render_with_layout(request,
                              'account_user',
                              'account_user_index.html',
                              ctx)


@login_required
@permission_required('gpermission.account_user_write')
@lock_required('account_user')
def user_add(request):
    if request.method == 'POST':
        uname = request.POST['username']
        email = request.POST['useremail']
        pwd = request.POST['userpassword']
        pwd2 = request.POST['userpassword2']
        if pwd != pwd2:
            messages.error(request, _('Password mismatch!'))
        else:
            UserModel.objects.create_user(uname, email, pwd)
    return redirect('account_user_index')


@login_required
@permission_required('gpermission.account_user_write')
@lock_required('account_user')
def user_delete(request):
    if request.method == 'POST':
        try:
            # user = UserModel.objects.get(username=request.POST['username'])
            # user.is_active = False
            # user.save()
            username = request.POST['username']
            UserModel.objects.filter(username=username).delete()
        except UserModel.DoesNotExist:
            messages.error(request,
                           (_('User does not exist:%s') % (username,)))
        except Exception as ex:
            messages.error(request, str(ex))
    return redirect('account_user_index')


@login_required
@permission_required('gpermission.account_group_read')
def group_index(request):
    groups = []
    grps = GroupModel.objects.all()
    for grp in grps:
        grpperms = [{
            "id": perm.id,
            "name": perm.name,
            "codename": perm.codename} for perm in grp.permissions.all()]
        grpusers = []
        for usr in UserModel.objects.all():
            for ugrp in usr.groups.all():
                if ugrp.id == grp.id:
                    grpusers.append({
                        "id": usr.id,
                        "name": usr.username})
        groups.append({
            "id": grp.id,
            "name": grp.name,
            "users": [u['name'] for u in grpusers],
            "permissions": [_(p['name']) for p in grpperms],
        })
    users = [{
        "id": u.id,
        "name": u.username
    } for u in UserModel.objects.filter(is_active=True, is_superuser=False)]
    perms = [{
        "id": p.id,
        "name": _(p.name),
        "codename": p.codename,
    } for p in all_permissions()]
    return render_with_layout(request,
                              'account_group',
                              'account_group_index.html',
                              {
                                  "groups": groups,
                                  "users": users,
                                  "permissions": perms,
                              })


@login_required
@permission_required('gpermission.account_group_write')
@lock_required('account_group')
def group_add(request):
    if request.method == 'POST':
        name = request.POST['groupname']
        group = GroupModel(name=name)
        group.save()
    return redirect('account_group_index')


@login_required
@permission_required('gpermission.account_group_write')
@lock_required('account_group')
def group_delete(request):
    if request.method == 'POST':
        id = int(request.POST['groupid'])
        # group = GroupModel.objects.get(id=id)
        GroupModel.objects.filter(id=id).delete()
    return redirect('account_group_index')


@login_required
@permission_required('gpermission.account_group_write')
@lock_required('account_group')
def group_set_users(request):
    if request.method == 'POST':
        print('body {}'.format(request.body))
        data = json.loads(request.body)
        group = GroupModel.objects.get(id=data['gid'])
        users = [UserModel.objects.get(id=uid) for uid in data['uids']]
        group.user_set.set(users)
        return response_json(payload=[{"id": u.id,
                                       "name": u.username} for u in users],
                             message=_('group users updated'))
    return response_json(code=-1,
                         messages=_('only support post request'))


@login_required
@permission_required('gpermission.account_group_write')
@lock_required('account_group')
def group_set_permissions(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        group = GroupModel.objects.get(id=data['gid'])
        perms = [PermissionModel.objects.get(id=pid) for pid in data['pids']]
        group.permissions.set(perms)
        return response_json(payload=[{"id": p.id,
                                       "name": _(p.name)} for p in perms],
                             message=_('group permissions updated'))
    return response_json(code=-1,
                         messages=_('only support post request'))


@login_required
@permission_required('gpermission.account_group_read')
def group_users(request):
    grp = GroupModel.objects.get(id=request.GET['gid'])
    users = [{
        "id": user.id,
        "name": user.username} for user in grp.user_set.all()]
    return response_json(payload=users)


@login_required
@permission_required('gpermission.account_group_read')
def group_permissions(request):
    grp = GroupModel.objects.get(id=request.GET['gid'])
    perms = [{
        "id": perm.id,
        "name": _(perm.name),
        "codename": perm.codename} for perm in grp.permissions.all()]
    return response_json(payload=perms)
