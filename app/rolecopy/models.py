# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Rolecopy(models.Model):
    serverId = models.IntegerField(default=1)
    fpId = models.BigIntegerField(default=0)
    roleId = models.BigIntegerField(default=0)
    fullData = models.TextField(default="")


def get_model(id):
    try:
        return Rolecopy.objects.get(id=id)
    except Exception:
        return {}


def add_model(serverId, fpId, roleId, fullData):
    m = Rolecopy(serverId=serverId, fpId=fpId, roleId=roleId, fullData=fullData)
    m.save()


def delete_model(id):
    try:
        return Rolecopy.objects.filter(id=id).delete()
    except Exception:
        return {}


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Rolecopy.objects.all()))])


def pull_role_data_from_game_server(serverId, fpId, roleId):
    from django.conf import settings
    from pypuzlib import httprpc as rpc
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('pull-role-data',
                                                         {'server_id': serverId, 'fp_id': fpId, 'role_id': roleId})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def push_role_data_to_game_server(serverId, fpId, roleId, fullData):
    from django.conf import settings
    from pypuzlib import httprpc as rpc
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('push-role-data', {'server_id': serverId,
                                                                            'fp_id': fpId,
                                                                            'role_id': roleId,
                                                                            'full_data': fullData})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}
