# coding: utf-8
import json
from django.contrib.auth.decorators import login_required, permission_required
from library import layout
from app.customservice import models
from django.shortcuts import render, redirect
from library.layout import render_with_layout, response_json
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from library.lock import lock_required


ELogModeType_name = {
    0 : ('Unity', _('Unity')),
    1 : ('Device', _('Device')),
    2 : ('Net', _('Net')),
    3 : ('Update', _('Update')),
    4 : ('Load', _('Load')),
	5 : ('Task', _('Task')),
    6 : ('Battle', _('Battle')),
    7 : ('Team', _('Team')),
	8 : ('Union', _('Union')),
	9 : ('Guide', _('Guide')),
    10 : ('Hero', _('Hero')),
    11 : ('Building', _('Building')),
    12 : ('Editor', _('Editor')),
    13 : ('Player', _('Player')),
    100 : ('Custom1', _('Custom1')),
    101 : ('Custom2', _('Custom2')),
    102 : ('Custom3', _('Custom3')),
    103 : ('Custom4', _('Custom4')),
    104 : ('Custom5', _('Custom5')),
    105 : ('Custom6', _('Custom6')),
    106 : ('Custom7', _('Custom7')),
    107 : ('Custom8', _('Custom8')),
    108 : ('Custom9', _('Custom9')),
    # (109, 'Count', _('Count')),
}


ELogOutputMode_name = {
    1 : ('Mode_None', _('Mode_None')),
    2 : ('Mode_File', _('Mode_File')),
    4 : ('Mode_Net', _('Mode_Net')),
}


EPLogLevel_name = {
    1 : ('ERROR', _('ERROR')),
    2 : ('ASSERT', _('ASSERT')),
    4 : ('WARNING', _('WARNING')),
    8 : ('LOG', _('LOG')),
    16 : ('EXCEPTION', _('EXCEPTION')),
}


EPlogOpenUnityLog = {
    0 : ('UnityLogClose', _('UnityLogClose')),
    1 : ('UnityLogOpen', _('UnityLogOpen')),
}


EPlogTag = {
    '*' : ('TagDefault', _('TagDefault')),
    'android' : ('TagAndroid', _('TagAndroid')),
    'ios' : ('TagIOS', _('TagIOS')),
}


@login_required
@permission_required('gpermission.userinfo_read')
def userinfo_index(request):
    return layout.render_with_layout(request, 'userinfo', 'userinfo_index.html')


@login_required
@permission_required('gpermission.plog_read')
def plog_index(request):
    all_settings = []
    settingsobj = models.get_plog_settings()
    for k, v in settingsobj.items():
        tag = ''
        obj = v['setting']
        if k in EPlogTag:
            tag = {'name': EPlogTag[k][1], 'id': k}
        else:
            tag = {'name': k, 'id': k}
        slogMod = []
        for modid in obj['enable_mode_list']:
            slogMod.append({'name':ELogModeType_name[modid][1],
                        'id':modid})
        output = []
        outsite = obj['log_output_site']
        for o in ELogOutputMode_name:
            if o & outsite != 0:
                output.append({'name':ELogOutputMode_name[o][1],
                            'id':o})
        level = []
        levelsite = obj['log_level_site']
        for o in EPLogLevel_name:
            if o & levelsite != 0:
                level.append({'name':EPLogLevel_name[o][1], 'id':o})
        
        
        all_mode_info = [{'id':k, 'name':v[1]} for k, v in ELogModeType_name.items()]

        all_out_put_info = [{'id':k, 'name':v[1]} for k, v in ELogOutputMode_name.items()]

        all_level_info = [{'id':k, 'name':v[1]} for k, v in EPLogLevel_name.items()]

        all_settings.append({
            "tag" : tag,
            "enable_mode_list" : slogMod,
            "log_level_site" : level,
            "log_output_site" : output,
            "unity_log_open" : {'name': EPlogOpenUnityLog[1][1], 'id': 1} if obj['unity_log_open'] else {'name': EPlogOpenUnityLog[0][1], 'id': 0}
        })
    return layout.render_with_layout(request, 'plog', 'plog_index.html', {"plogsettings":all_settings, "outputtype":all_out_put_info, 
            "logleveltype":all_level_info, 
            "modetype":all_mode_info})


@login_required
@permission_required('gpermission.plog_write')
@lock_required('plog')
def plog_delete(request):
    if request.method == 'POST':
        tag = request.POST['tag']
        code, message ,payload = models.plog_delete(tag=tag)
        if code != 0:
            messages.error(request,message)

        return redirect('plog_index')


@login_required
@permission_required('gpermission.plog_write')
@lock_required('plog')
def plog_update(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        plogSetting = data['plogSetting']
        if 'tag' not in plogSetting.keys() or plogSetting['tag']['id'] == '':
            messages.error(request,"tag not be empty")
            return 
        enable_mode_list = []
        log_level_site = 0
        log_output_site = 0
        unity_log_open = False
        for obj in plogSetting['enable_mode_list']:
            enable_mode_list.append(obj['id'])
        for obj in plogSetting['log_output_site']:
            log_output_site |= obj['id']
        for obj in plogSetting['log_level_site']:
            log_level_site |= obj['id']
        if plogSetting['unity_log_open']['id'] == 1:
            unity_log_open = True
        
        resp = models.update_plog_setting({'tag':plogSetting['tag']['id'], 'setting':{ 'enable_mode_list':enable_mode_list,
                                    'log_level_site':log_level_site, 'log_output_site':log_output_site,
                                    'unity_log_open':unity_log_open}})

        return response_json(payload=resp)