# coding:utf-8

import json


from django.core import serializers
from django.db import models

from django.conf import settings
from pypuzlib import httprpc as rpc


class Sealfpid(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Sealfpid.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Sealfpid(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Sealfpid.objects.all()))])


def get_seal_fpid():
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('getSealByFpId', [])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def mod_seal_fpid(fpid, op, expired_time):
    req = {'fp_id': fpid, 'append_or_remove': op, 'expired_time': expired_time}
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('sendSealModFpId', req)
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def timestampToTime(timestamp):
    from datetime import datetime

    timeArray = datetime.utcfromtimestamp(timestamp)
    result = timeArray.strftime("%Y-%m-%d %H:%M:%S")
    return result


def timeToTimestamp(str_time):
    from datetime import datetime
    import time

    timeArray = datetime.strptime(str_time, '%Y-%m-%d %H:%M:%S').utctimetuple()

    # make time
    timestamp = int(time.mktime(timeArray)) * 1000
    return timestamp
