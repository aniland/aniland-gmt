# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Mail(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Mail.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Mail(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Mail.objects.all()))])
