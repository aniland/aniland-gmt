# coding: utf-8

from copy import deepcopy
from functools import reduce


def deep_merge_dict(*dicts):
    def merge_into(d1, d2):
        for key in d2:
            if key not in d1 or not isinstance(d1[key], dict):
                d1[key] = deepcopy(d2[key])
            else:
                d1[key] = merge_into(d1[key], d2[key])
        return d1
    return reduce(merge_into, dicts, {})
