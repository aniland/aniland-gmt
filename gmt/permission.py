# coding: utf-8

from django.utils.translation import gettext_lazy as _
from library.config.permission import PERMISSIONS as LIBPERMISSIONS


PERMISSIONS = LIBPERMISSIONS + [
    ('sandbox_read', _('Sandbox Read')),
    ('sandbox_write', _('Sandbox Write')),

    ('gds_read', _('GDS Read')),
    ('gds_write', _('GDS Write')),

    ('event_read', _('Event Read')),
    ('event_write', _('Event Write')),

    ('userinfo_read', _('User Info Read')),
    ('userinfo_write', _('User Info Write')),

    ('plog_read', _('PLog Setting Read')),
    ('plog_write', _('PLog Setting Write')),

    ('userprobe_read', _('User Probe Read')),
    ('userprobe_write', _('User Probe Write')),

    ('mail_read', _('Mail Read')),
    ('mail_write', _('Mail Write')),

    ('qualitySetting_read', _('Qualitysetting Read')),
    ('qualitySetting_write', _('Qualitysetting Write')),

    ('whitelistfpid_read', _('Whitelistfpid Read')),
    ('whitelistfpid_write', _('Whitelistfpid Write')),

    ('whitelistip_read', _('Whitelistip Read')),
    ('whitelistip_write', _('Whitelistip Write')),

    ('sealfpid_read', _('Sealfpid Read')),
    ('sealfpid_write', _('Sealfpid Write')),

    ('serverstatus_read', _('Serverstatus Read')),
    ('serverstatus_write', _('Serverstatus Write')),

    ('globalSvrAddr_read', _('Globalsvraddr Read')),
    ('globalSvrAddr_write', _('Globalsvraddr Write')),

    ('globalRoutingRules_read', _('Globalroutingrules Read')),
    ('globalRoutingRules_write', _('Globalroutingrules Write')),

    ('kickplayer_read', _('Kickplayer Read')),
    ('kickplayer_write', _('Kickplayer Write')),

    ('deleteuser_read', _('Deleteuser Read')),
    ('deleteuser_write', _('Deleteuser Write')),

    ('rolecopy_read', _('Rolecopy Read')),
    ('rolecopy_write', _('Rolecopy Write')),

    ('GmtTest_read', _('Gmttest Read')),
    ('GmtTest_write', _('Gmttest Write')),
]
