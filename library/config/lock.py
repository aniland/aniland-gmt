# coding:utf-8

from django.utils.translation import gettext_lazy as _

LOCKS = [
    (
        'account_user',
        'gpermission.account_user_write',
        _('User')
    ),
    (
        'account_group',
        'gpermission.account_group_write',
        _('Group')
    ),
    (
        'metadata_management',
        'gpermission.metadata_management_write',
        _('Metadata')
    ),
]
