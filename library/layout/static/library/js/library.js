/* ------------------------------------------------------------------------------
 *  Library Global JS functions
 * ---------------------------------------------------------------------------- */

/* customize 3rd-party libraries */

$.fn.editable.defaults.mode = 'inline';


/* library functions  */

var C = {
	debug: false,
	application: '',
	environment: '',

	username: '',
	module: {},
	lock: {},
	metadata: {},

	crud: {
		url_create: '',
		url_delete: '',
		url_update: '',
		url_query: '',
		url_query_all: '',
	},
};

var L = function() {
	var _popupMessage = function(level, message) {
		var html = '                                                   \
<div id=\"js-alert-message\" class=\"col-md-8 offset-md-4\">       \
<div id=\"js-card-alert-message\" style=\"display:none;\">         \
<div class=\"alert ATTRIBUTE\" role=\"alert\">                         \
[LEVEL] MESSAGE                                                    \
</div>                                                             \
</div>                                                             \
</div>                                                             \
		';
		attrStr = "";
		if(level == "error") {
			attrStr = "alert-danger";
		} else if(level == "warning") {
			attrStr = "alert-warning";
		} else if(level == "success") {
			attrStr = "alert-success";
		} else if(level == "info") {
			attrStr = "alert-primary";
		} else {
			attrStr = "alert-secondary";
		}
		html = html.replace(/LEVEL/g, level).
								replace(/MESSAGE/g, message).
								replace(/ATTRIBUTE/g, attrStr);
		$('#content-messages-area').html(html);
		$('#js-card-alert-message').show();
		$('#js-card-alert-message').
										 addClass('animated fadeInRight').
										 one('webkitAnimationEnd ' +
												 'mozAnimationEnd ' +
												 'MSAnimationEnd ' +
												 'oanimationend ' +
												 'animationend', function () {
													 $(this).removeClass('animated fadeInRight');
													 setTimeout(function() {
														 $('#js-card-alert-message').
																	 addClass('animated fadeOutRight').
																	 one('webkitAnimationEnd ' +
																			 'mozAnimationEnd ' +
																			 'MSAnimationEnd ' +
																			 'oanimationend ' +
																			 'animationend', function () {
																				 $(this).removeClass('animated fadeOutRight');
																				 $('#js-card-alert-message').hide();
																				 $('#js-alert-message').remove();
																			 });
													 }, 4000);
												 });
	};

	var _getJson = function(url, data, cb) {
		$.ajax({
			url: url,
			method: "GET",
			data: data,
			dataType: "json",
			cache: false,
			success: function(response, statud, xhr) {
				if(response.code != 0) {
					_popupMessage("error",
												"" + response.code + "|" + (response.message||""));
				} else {
					if(response.message) {
						_popupMessage("success", response.message)
					}
					if(cb) {
						cb(response.payload);
					}
				}
			},
			error: function(xhr, status, error) {
				_popupMessage("error", "xhr:" + xhr + ', ' + status + ", " + error);
			}
		});
	};

	var	_postJson = function(url, data, cb) {
		$.ajax({
			url: url,
			method: "POST",
			cache: false,
			data: JSON.stringify(data),
			dataType: "json",
			contentType: 'application/json; charset=utf-8',
			headers:{"X-CSRFToken": Cookies.get('csrftoken'),},
			success: function(response, statud, xhr) {
				if(response.code != 0) {
					_popupMessage("error",
												"" + response.code + "|" + (response.message||""));
				} else {
					if(response.message) {
						_popupMessage("success", response.message)
					}
					if(cb) {
						cb(response.payload);
					}
				}
			},
			error: function(xhr, status, error) {
				_popupMessage("error", "xhr:" + xhr + ', ' + status + ", " + error);
			}
		});
	};

	var _rpc = function(module, client, id, data, cb) {
		_postJson('httprpc', {
			'module': module,
			'client': client,
			'id': id,
			'data': data,
		}, cb)
	}

	var _download = function(url, data, name) {
		$.ajax({
			url: url,
			method: "GET",
			data: data,
			dataType: 'binary',
			cache: false,
			xhrFields: { responseType: 'blob' },
			success: function(response, statud, xhr) {
				$('#library-download-links').children().remove();
				var url = URL.createObjectURL(response);
				var $a = $('<a />', {
          'href': url,
          'download': name,
          'text': name,
        }).hide().appendTo($("#library-download-links"))[0].click();
			},
			error: function(xhr, status, error) {
				_popupMessage("error", "xhr:" + xhr + ', ' + status + ", " + error);
			}
		});
	}

	var _manualForm = function(action, method, params) {
		var form = document.createElement("form");
		form.style = "display:none;";
		form.method = method;
		form.action = action;
		Object.keys(params).forEach(function(k) {
			var input = document.createElement("input");
			input.type = "text";
			input.name = k;
			input.value = params[k];
			form.appendChild(input);
		});
		if(method == 'POST' || method == 'post') {
			var input = document.createElement("input");
			input.type = 'hidden';
			input.name = 'csrfmiddlewaretoken';
			input.value = Cookies.get('csrftoken');
			form.appendChild(input);
		}

		$('#library-manual-forms').append(form);
		form.submit();
		$('#library-manual-forms').children().remove();
	}

	var _editable_url = function(params, cb) {
		var d = new $.Deferred();

		if(!params || !params.crud || !params.id || !params.fields) {
			var msg = "missing params:" + JSON.stringify(params);
			_popupMessage("error", msg);
			return d.reject(msg);
		}
		
		$.ajax({
			url: C.crud.url_update,
			method: "POST",
			cache: false,
			data: JSON.stringify(params),
			dataType: "json",
			contentType: 'application/json; charset=utf-8',
			headers:{"X-CSRFToken": Cookies.get('csrftoken'),},
			success: function(response, status, xhr) {
				if(response.code != 0) {
					var msg = "" + response.code + "|" + (response.message||"");
					_popupMessage("error", msg);
					return d.reject(msg);
				}
				
				if(response.message) {
					_popupMessage("success", response.message)
				}
				if(cb) {
					cb(response.payload);
				}

				return d.resolve();
			},
			error: function(xhr, status, error) {
				var msg = "xhr:" + xhr + ', ' + status + ", " + error;
				_popupMessage("error", msg);
				return d.reject(msg);
			},
		});

		return d.promise();
	}
	
	var _editable_params = function(elem, params) {
		var data = {}
		data['crud'] = elem.attr('data-crud');
		data['id'] = params.pk || elem.attr('data-pk');
		data['fields'] = {};
		data['fields'][params.name] = params.value;
		return data;
	}
	
	return {

		message: _popupMessage,

		alert: function(message) {
			_popupMessage("error", message);
		},

		info: function(message) {
			_popupMessage("info", message);
		},
		
		getJson: _getJson,

		postJson: _postJson,

		rpc: _rpc,

		download: _download,

		form: _manualForm,

		crudCreate: function(id, fields, cb) {
			var elem = $('#' + id);
			value = {
				'crud': elem.attr("crud-module"),
				'fields': fields,
			};
			_postJson(C.crud.url_create, value, cb);
		},

		crudDelete: function(id, cb) {
			var elem = $('#' + id);
			value = {
				'crud': elem.attr("crud-module"),
				'id': elem.attr("crud-id"),
			};
			_postJson(C.crud.url_delete, value, cb);
		},

		crudUpdate: function(id, fields, cb) {
			var elem = $('#' + id);
			value = {
				'crud': elem.attr("crud-module"),
				'id': elem.attr("crud-id"),
				'fields': fields,
			};
			_postJson(C.crud.url_update, value, cb);
		},

		crudQuery: function(id, cb) {
			var elem = $('#' + id);
			value = {
				'crud': elem.attr('crud-module'),
				'id': elem.attr("crud-id"),
			};
			_getJson(C.crud.url_query, value, cb);
		},

		crudQueryAll: function(id, cb) {
			var elem = $('#' + id);
			value = {
				'crud': elem.attr('crud-module'),
			};
			_getJson(C.crud.url_query_all, value, cb);
		},

		editable: function(elem, options, cb) {
			if(elem == null) return;

			if(elem instanceof HTMLElement) {
				elem = $(elem);
			}

			elem.editable(Object.assign({
				url: function (params) {
					return _editable_url(params, cb);
				},
				params: function (params) {
					return _editable_params(elem, params);
				},
			}, options));
		},

		editable_submit: function(elemId, cb) {
			var elem = $('#' + elemId);
			if(elem == null) return;

			elem.submit({
				url: function(params) {
					return _editable_url(params, cb);
				},
				data: _editable_params(elem, params),
			});
		},

		version: "0.0.1"
	};
}();
