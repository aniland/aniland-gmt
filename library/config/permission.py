# coding: utf-8

from django.utils.translation import gettext_lazy as _


PERMISSIONS = [
    ('lock_read', _('Lock Read')),
    ('lock_write', _('Lock Write')),

    ('account_user_read', _('Account User Read')),
    ('account_user_write', _('Account User Write')),

    ('account_group_read', _('Account Group Read')),
    ('account_group_write', _('Account Group Write')),

    ('metadata_management_read', _('Metadata Management Read')),
    ('metadata_management_write', _('Metadata Management Write')),
]
