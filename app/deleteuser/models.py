# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Deleteuser(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Deleteuser.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Deleteuser(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Deleteuser.objects.all()))])


def delete_user(fpId,roleId):
    from django.conf import settings
    from pypuzlib import httprpc as rpc
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('delete-user', {'fp_id': fpId, 'role_id': roleId, 'deleted': 1})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}
