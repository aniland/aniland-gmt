# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.module import (MODULES as LIBMODULES, merge_modules)


MODULES = merge_modules([
    {
        'name': 'game',
        'title': _('Game'),
        'icon': 'fab fa-playstation',
        'module': [
            {
                'name': 'gds',
                'title': _('GDS'),
                'icon': 'fas fa-table',
                'url': 'gds_index',
                'permission': 'gpermission.gds_read',
                'lock': 'gds',
            },
        ]
    },
    {
        'name': 'operation',
        'title': _('Operation'),
        'icon': 'fas fa-chalkboard-teacher',
        'module': [
            {
                'name': 'event',
                'title': _('Event'),
                'icon': 'fas fa-calendar-alt',
                'url': 'event_index',
                'permission': 'gpermission.event_read',
                'lock': 'event',
            },
            {
                'name': 'serverstatus',
                'title': _('Serverstatus'),
                'icon': 'fas fa-book',
                'url': 'serverstatus_index',
                'permission': 'gpermission.serverstatus_read',
                'lock': 'serverstatus',
                'metadata': 'serverstatus',
            },
            {
                'name': 'whitelistip',
                'title': _('Whitelistip'),
                'icon': 'fas fa-book',
                'url': 'whitelistip_index',
                'permission': 'gpermission.whitelistip_read',
                'lock': 'whitelistip',
                # 'metadata': 'whitelistip',
            },
            {
                'name': 'whitelistfpid',
                'title': _('Whitelistfpid'),
                'icon': 'fas fa-book',
                'url': 'whitelistfpid_index',
                'permission': 'gpermission.whitelistfpid_read',
                'lock': 'whitelistfpid',
            },
            {
                'name': 'globalRoutingRules',
                'title': _('Globalroutingrules'),
                'icon': 'fas fa-book',
                'url': 'globalRoutingRules_index',
                'permission': 'gpermission.globalRoutingRules_read',
                'lock': 'globalRoutingRules',
                #'metadata': 'globalRoutingRules',
            },
            {
                'name': 'globalSvrAddr',
                'title': _('Globalsvraddr'),
                'icon': 'fas fa-book',
                'url': 'globalSvrAddr_index',
                'permission': 'gpermission.globalSvrAddr_read',
                'lock': 'globalSvrAddr',
                #'metadata': 'globalSvrAddr',
            },
        ]
    },
    {
        'name': 'custom_service',
        'title': _('Custom Service'),
        'icon': 'fab fa-napster',
        'module': [
            {
                'name': 'userinfo',
                'title': _('User Info'),
                'icon': 'fas fa-user-md',
                'url': 'userinfo_index',
                'permission': 'gpermission.userinfo_read',
                'lock': 'userinfo',
            },
            {
                'name': 'plog',
                'title': _('User PLog Setting'),
                'icon': 'fas fa-vial',
                'url': 'plog_index',
                'permission': 'gpermission.plog_read',
                'lock': 'plog',
            },
            {
                'name': 'mail',
                'title': _('Mail'),
                'icon': 'fas fa-book',
                'url': 'mail_index',
                'permission': 'gpermission.mail_read',
            },
            {
                'name': 'qualitySetting',
                'title': _('Qualitysetting'),
                'icon': 'fas fa-book',
                'url': 'qualitySetting_index',
                'permission': 'gpermission.qualitySetting_read',
                'lock': 'qualitySetting',
                'metadata': 'qualitySetting',
            },
            {
                'name': 'sealfpid',
                'title': _('Sealfpid'),
                'icon': 'fas fa-book',
                'url': 'sealfpid_index',
                'permission': 'gpermission.sealfpid_read',
                'lock': 'sealfpid',
                # 'metadata': 'sealfpid',
            },
            {
                'name': 'kickplayer',
                'title': _('Kickplayer'),
                'icon': 'fas fa-book',
                'url': 'kickplayer_index',
                'permission': 'gpermission.kickplayer_read',
                'lock': 'kickplayer',
                #'metadata': 'kickplayer',
            },
            {
                'name': 'deleteuser',
                'title': _('Deleteuser'),
                'icon': 'fas fa-book',
                'url': 'deleteuser_index',
                'permission': 'gpermission.deleteuser_read',
                'lock': 'deleteuser',
                #'metadata': 'deleteuser',
            },
            {
                'name': 'rolecopy',
                'title': _('Rolecopy'),
                'icon': 'fas fa-book',
                'url': 'rolecopy_index',
                'permission': 'gpermission.rolecopy_read',
                'lock': 'rolecopy',
                #'metadata': 'rolecopy',
            },
        ],
    },
    {
        'name': 'testing',
        'title': _('Testing'),
        'icon': 'fas fa-bug',
        'module': [
            {
                'name': 'userprobe',
                'title': _('User Probe'),
                'icon': 'fas fa-blind',
                'url': 'userprobe_index',
                'permission': 'gpermission.userprobe_read',
                'lock': 'userprobe',
            },
        ],
    },
    {
        'name': 'development',
        'module': [
            {
                'name': 'sandbox',
                'title': _('Sandbox'),
                'icon': 'fas fa-bong',
                'url': 'sandbox_index',
                'permission': 'gpermission.sandbox_read',
                'lock': 'sandbox',
                'metadata': 'sandbox',
            },
        ],
    },

    {
        'name': 'GmtTest',
        'title': _('Gmttest'),
        'icon': 'fas fa-briefcase',
        'module': [
            {
                'name': 'GmtTest',
                'title': _('Gmttest'),
                'icon': 'fas fa-book',
                'url': 'GmtTest_index',
                'permission': 'gpermission.GmtTest_read',
                'lock': 'GmtTest',
                'metadata': 'GmtTest',
            },
        ],
    },
], LIBMODULES)
