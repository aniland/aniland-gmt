# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.whitelistip import models

from enum import Enum


class WhitelistOp(Enum):
    APPEND = 1
    REMOVE = 2


@login_required
@permission_required('gpermission.whitelistip_read')
def index(request):
    resp = models.get_whitelist_by_ip()

    ctx = {}
    ctx['dataList'] = resp

    return layout.render_with_layout(request,
                                     'whitelistip',
                                     'whitelistip_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.whitelistip_write')
@lock_required('whitelistip')
def addone(request):
    if request.method == 'POST':
        print(request.POST)

        ipAddr = request.POST['ip']
        resp = models.mod_whitelist_ip(ipAddr, WhitelistOp.APPEND.value)
        ctx = {'dataList': resp}
        return layout.render_with_layout(request,
                                     'whitelistip',
                                     'whitelistip_index.html',
                                     ctx)

@login_required
@permission_required('gpermission.whitelistip_write')
@lock_required('whitelistip')
def delete(request):
    if request.method == 'POST':
        print(request.POST)

        ipAddr = request.POST['ip']
        resp = models.mod_whitelist_ip(ipAddr, WhitelistOp.REMOVE.value)
        ctx = {'dataList': resp}
        return layout.render_with_layout(request,
                                     'whitelistip',
                                     'whitelistip_index.html',
                                     ctx)