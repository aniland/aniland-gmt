# coding: utf-8

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.sandbox.models import Sandbox


@login_required
@permission_required('gpermission.sandbox_read')
def index(request):
    sd = list(Sandbox.objects.all())
    ctx = {'sandbox': {}}
    if sd:
        ctx['sandbox']['id'] = sd[0].id
        ctx['sandbox']['name'] = sd[0].name
        ctx['sandbox']['hash'] = sd[0].hash
        ctx['sandbox']['level'] = sd[0].level
    return layout.render_with_layout(request,
                                     'sandbox',
                                     'sandbox_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.sandbox_write')
@lock_required('sandbox')
def save(request):
    oid = request.POST['id']
    if oid:
        sd = Sandbox(id=oid,
                     name=request.POST['name'],
                     hash=request.POST['hash'],
                     level=request.POST['level'])
    else:
        sd = Sandbox(name=request.POST['name'],
                     hash=request.POST['hash'],
                     level=request.POST['level'])
    sd.save()
    return redirect(request.POST['redirect'])
