# coding:utf-8

from functools import wraps
from django.conf import settings
from django.shortcuts import redirect
from django.contrib import messages


def has_lock(username, lockname):
    from library.lock.models import Lock as LockModel

    try:
        lock = LockModel.objects.get(name=lockname)
    except LockModel.DoesNotExist:
        lock = None

    return lock and lock.locked_by == username


def lock_owner(lockname):
    from library.lock.models import Lock as LockModel

    try:
        lock = LockModel.objects.get(name=lockname)
    except LockModel.DoesNotExist:
        lock = None

    return lock.locked_by if lock else ""


def lock(lockname, user):
    from library.lock.models import lock
    return lock(lockname, user)


def unlock(lockname, user):
    from library.lock.models import unlock
    return unlock(lockname, user)


def _lock_error(request, lockname, error_message, redirect_url):
    messages.error(request, error_message)
    return redirect(redirect_url)


def lock_required(lockname, redirect_url=None, error_callback=_lock_error):
    from library.lock.models import Lock as LockModel

    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            redirect = redirect_url
            if not redirect:
                redirect = settings.LOGIN_URL
            try:
                lock = LockModel.objects.get(name=lockname)
            except LockModel.DoesNotExist:
                lock = None
            if not lock:
                return error_callback(request,
                                      lockname,
                                      ('%s has not been locked' % lockname),
                                      redirect)
            if lock.locked_by != request.user.username:
                return error_callback(request,
                                      lockname,
                                      ('%s locked by %s' % (lockname,
                                                            lock.locked_by)),
                                      redirect)
            return func(request, *args, **kwargs)
        return wrapper
    return decorator
