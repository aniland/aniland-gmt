# coding: utf-8

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from library import layout


@login_required
def home(request):
    return layout.render_with_layout(request, '', 'home.html')


@login_required
def navback(request):
    return redirect(request.META.get('HTTP_REFERER', '/'))
