# coding:utf-8

import json
from django.core import serializers
from django.shortcuts import render
from django.conf import settings
from django.http import (HttpResponse, JsonResponse)
from django.utils.module_loading import import_string


MODULES = import_string(settings.MODULE_CONFIG) if \
    hasattr(settings, 'MODULE_CONFIG') and settings.MODULE_CONFIG \
    else []


def response_file(content, name, contenttype):
    response = HttpResponse(content, content_type=contenttype)
    response['Content-Disposition'] = 'attachment; filename=' + name
    return response


def response_text_file(content, name):
    return response_file(content, name, 'text/plain; charset=utf-8')


def response_bin_file(content, name):
    return response_file(content, name, 'application/octet-stream')


def response_json_file(content, name, compact=True):
    if not isinstance(content, str):
        if compact:
            data = json.dumps(content, indent=None, separators=(',', ':'))
        else:
            data = json.dumps(content, indent=4, separators=(', ', ': '))
    else:
        data = content
    return response_file(data, name, 'application/json; charset=utf-8')


def response_json(payload={}, code=0, message=""):
    return JsonResponse({
        'code': code,
        'message': message,
        'payload': payload,
    })


def response_json_model(model=None, code=0, message=""):
    data = json.loads(serializers.serialize("json", model))
    if isinstance(data, (tuple, list)):
        payload = []
        if data:
            for d in data:
                value = d['fields']
                if 'id' not in value:
                    value['id'] = d['pk']
                payload.append(value)
    else:
        payload = {}
        if data:
            payload = data['fields']
            if 'id' not in payload:
                payload['id'] = data['pk']

    return response_json(payload, code, message)


def render_with_layout(request,
                       module_name,
                       template_name,
                       context=None,
                       content_type=None,
                       status=None,
                       using=None):
    commonctx = _get_common_context(request)

    bannerctx = _get_banner_area_context(request)
    navigationctx = _get_navigation_context(request)
    headerctx = _get_content_header_context(request)
    footerctx = _get_content_footer_context(request)

    modulectx, module = _get_module_context(request, module_name)
    lockctx = _get_lock_context(request, module)
    metadatactx = _get_metadata_context(request, module)

    contentctx = context if context else {}

    ctx = {
        **commonctx,

        **bannerctx,
        **navigationctx,
        **headerctx,
        **footerctx,

        **modulectx,
        **lockctx,
        **metadatactx,

        **contentctx,
    }
    return render(request,
                  template_name,
                  ctx,
                  content_type,
                  status,
                  using)


def _get_common_context(request):
    return {
        'application': settings.APPLICATION.capitalize(),
        'environment': settings.ENVIRONMENT.capitalize(),
        'debug': settings.DEBUG,
        'app_global_assets': getattr(settings, 'APP_GLOBAL_ASSETS', '')
    }


def _get_lock_context(request, module):
    from library.lock.models import (get_lock_config, Lock as LockModel)

    lock_name = module.get('lock', '')
    # lock_redirect = module.get('url', '')
    try:
        lock_owner = LockModel.objects.get(name=lock_name)
    except LockModel.DoesNotExist:
        lock_owner = None

    if lock_name:
        lock_locked = True
        if lock_owner and lock_owner.locked_by == request.user.username:
            lock_locked = False

        lock_no_permission = True
        item = get_lock_config(lock_name)
        if item and request.user.has_perm(item[1]):
            lock_no_permission = False
    else:
        lock_locked = False
        lock_no_permission = False

    return {
        'lock': {
            'name': lock_name,  # no lock_name means lock is not required
            # 'redirect': lock_redirect,
            'no_permission': lock_no_permission,
            'status_attr': 'disabled' if lock_locked else '',
            'owner': lock_owner.locked_by if lock_owner else '',
        }
    }


def _get_metadata_context(request, module):
    import library.metadata.models as MDModels

    metadata_name = module.get('metadata', '')
    if metadata_name:
        md_versions = sorted(MDModels.get_metadata_list(metadata_name),
                             key=lambda v: v.time,
                             reverse=True)

        processed = MDModels.generate_metadata_processed(metadata_name)
        md_current_hash = MDModels.generate_metadata_hash(processed)

        md_live = MDModels.get_live_metadata(metadata_name)
        md_live_hash = md_live['hash'] if md_live else ''

        current_metadata_id = 0
        live_metadata_id = 0
        for md in md_versions:
            if md and md.hash == md_current_hash:
                current_metadata_id = md.id
            if md and md.hash == md_live_hash:
                live_metadata_id = md.id
    else:
        md_versions = []
        md_current_hash = ''
        md_live_hash = ''
        current_metadata_id = 0
        live_metadata_id = 0

    return {
        'metadata': {
            'name': metadata_name,
            'live_hash': md_live_hash,
            'current_hash': md_current_hash,
            'versions': md_versions,
            'current_metadata_id': current_metadata_id,
            'live_metadata_id': live_metadata_id,
        }
    }


def _get_module_context(request, module_name):
    def _fetch_layout(request, group, module_name):
        layouts = []
        module = {}
        breadcrumbs = []

        mm = None
        gm = None
        for item in group:
            group_result = []
            module_result = []
            if 'module' in item:
                for m in item['module']:
                    if not m.get('permission', ''):
                        module_result.append(m)
                    if request.user.has_perm(m['permission']):
                        module_result.append(m)
                for m in module_result:
                    if m['name'] == module_name and not mm:
                        mm = m
                        breadcrumbs.append({
                            'title': m['title'],
                            'url': m['url'],
                        })

            if 'group' in item:
                group_result, m, bc = _fetch_layout(request,
                                                    item['group'],
                                                    module_name)
                if m and not gm and not mm:
                    gm = m
                    breadcrumbs += bc

            if module_result or group_result:
                item_result = {
                    'name': item['name'],
                    'title': item['title'],
                    'icon': item['icon'],
                }
                item_result['module'] = module_result if module_result else []
                item_result['group'] = group_result if group_result else []

                if (mm or gm) and not module:
                    module = mm if mm else gm
                    breadcrumbs.append({
                        'title': item['title'],
                        'url': '#',
                    })

                layouts.append(item_result)

        return layouts, module, breadcrumbs

    layouts, module, breadcrumbs = _fetch_layout(request, MODULES, module_name)

    return {
        'module': {
            'name': module_name if module else '',
            'layouts': layouts,
            'breadcrumbs': list(reversed(breadcrumbs)),
        }
    }, module


def _get_banner_area_context(request):
    return {
        'page_banner': {}
    }


def _get_navigation_context(request):
    return {
        'page_navigation': {}
    }


def _get_content_header_context(request):

    return {
        'page_header': {}
    }


def _get_content_footer_context(request):
    return {
        'page_footer': {}
    }


def get_module(request, name):
    _, m = _get_module_context(request, name)
    return m
