# coding: utf-8

from django.http import (HttpResponseNotAllowed, HttpResponseBadRequest)
from django.contrib.auth.decorators import (login_required,
                                            permission_required)
from django.shortcuts import redirect
from django.contrib import messages
from library import layout
from library.lock import models
from library.lock.models import Lock as LockModel


@login_required
@permission_required('gpermission.lock_read')
def index(request):
    locks = LockModel.objects.all()
    ctx = {'locks': locks if locks else []}
    return layout.render_with_layout(request, 'lock', 'lock_index.html', ctx)


@login_required
@permission_required('gpermission.lock_write')
def release(request):
    if request.method == 'POST':
        lockname = request.POST['lockname']
        LockModel.objects.filter(name=lockname).delete()
    return redirect('lock_index')


@login_required
@permission_required('gpermission.lock_write')
def release_all(request):
    if request.method == 'POST':
        LockModel.objects.all().delete()
    return redirect('lock_index')


@login_required
def lock(request):
    if request.method != 'POST':
        return HttpResponseNotAllowed

    lockname = request.POST['lockname']
    redirect_to = request.POST['redirect']
    if not lockname or not redirect_to:
        return HttpResponseBadRequest

    ok, msg = models.lock(lockname, request.user)
    if not ok:
        messages.error(request, msg)
    return redirect(redirect_to)


@login_required
def unlock(request):
    if request.method != 'POST':
        return HttpResponseNotAllowed

    lockname = request.POST['lockname']
    redirect_to = request.POST['redirect']
    if not lockname or not redirect_to:
        return HttpResponseBadRequest

    models.unlock(lockname, request.user)
    return redirect(redirect_to)
