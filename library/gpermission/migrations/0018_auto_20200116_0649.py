# Generated by Django 2.1.10 on 2020-01-16 06:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gpermission', '0017_merge_20191227_0839'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gpermission',
            options={'managed': False, 'permissions': [('lock_read', 'Lock Read'), ('lock_write', 'Lock Write'), ('account_user_read', 'Account User Read'), ('account_user_write', 'Account User Write'), ('account_group_read', 'Account Group Read'), ('account_group_write', 'Account Group Write'), ('metadata_management_read', 'Metadata Management Read'), ('metadata_management_write', 'Metadata Management Write'), ('sandbox_read', 'Sandbox Read'), ('sandbox_write', 'Sandbox Write'), ('gds_read', 'GDS Read'), ('gds_write', 'GDS Write'), ('event_read', 'Event Read'), ('event_write', 'Event Write'), ('userinfo_read', 'User Info Read'), ('userinfo_write', 'User Info Write'), ('plog_read', 'PLog Setting Read'), ('plog_write', 'PLog Setting Write'), ('userprobe_read', 'User Probe Read'), ('userprobe_write', 'User Probe Write'), ('mail_read', 'Mail Read'), ('mail_write', 'Mail Write'), ('qualitySetting_read', 'Qualitysetting Read'), ('qualitySetting_write', 'Qualitysetting Write'), ('whitelistfpid_read', 'Whitelistfpid Read'), ('whitelistfpid_write', 'Whitelistfpid Write'), ('whitelistip_read', 'Whitelistip Read'), ('whitelistip_write', 'Whitelistip Write'), ('sealfpid_read', 'Sealfpid Read'), ('sealfpid_write', 'Sealfpid Write'), ('serverstatus_read', 'Serverstatus Read'), ('serverstatus_write', 'Serverstatus Write'), ('globalSvrAddr_read', 'Globalsvraddr Read'), ('globalSvrAddr_write', 'Globalsvraddr Write'), ('globalRoutingRules_read', 'Globalroutingrules Read'), ('globalRoutingRules_write', 'Globalroutingrules Write'), ('kickplayer_read', 'Kickplayer Read'), ('kickplayer_write', 'Kickplayer Write'), ('deleteuser_read', 'Deleteuser Read'), ('deleteuser_write', 'Deleteuser Write'), ('rolecopy_read', 'Rolecopy Read'), ('rolecopy_write', 'Rolecopy Write')]},
        ),
    ]
