# coding:utf-8

from django.urls import path, include
from django.contrib.auth import views as authviews
from django.views.i18n import JavaScriptCatalog
from library.account import views as accountviews
from library.lock import views as lockviews
from library.crud import views as crudviews
from library.metadata import views as metadataviews
from library.layout import views as layoutviews
from app.customservice import views as customserviceviews

from library.httprpc import views as HttpRpcViews

urlpatterns = [
    # home
    path('', layoutviews.home, name='home'),

    # i18n
    path('i18n/', include('django.conf.urls.i18n')),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),

    # js rpc
    path('httprpc', HttpRpcViews.rpc, 'httprpc'),

    # account
    path('account/login/',
         authviews.LoginView.as_view(),
         name='login'),
    path('account/logout/',
         authviews.LogoutView.as_view(),
         name='logout'),
    path('account/password_change/',
         accountviews.change_password,
         name='password_change'),
    path('account/user/index',
         accountviews.user_index,
         name='account_user_index'),
    path('account/user/add',
         accountviews.user_add,
         name='account_user_add'),
    path('account/user/delete',
         accountviews.user_delete,
         name='account_user_delete'),
    path('account/group/index',
         accountviews.group_index,
         name='account_group_index'),
    path('account/group/add',
         accountviews.group_add,
         name='account_group_add'),
    path('account/group/delete',
         accountviews.group_delete,
         name='account_group_delete'),
    path('account/group/setusers',
         accountviews.group_set_users,
         name='account_group_setusers'),
    path('account/group/setpermissions',
         accountviews.group_set_permissions,
         name='account_group_setpermissions'),
    path('account/group/users',
         accountviews.group_users,
         name='account_group_users'),
    path('account/group/permissions',
         accountviews.group_permissions,
         name='account_group_permissions'),
     
     #plog setting
     path('custom/plogsetting/delete',
          customserviceviews.plog_delete,
          name='log_setting_delete'),
     path('custom/plogsetting/update',
          customserviceviews.plog_update,
          name='log_setting_update'),

    # lock
    path('lock/index', lockviews.index, name='lock_index'),
    path('lock/release', lockviews.release, name='lock_release'),
    path('lock/release_all', lockviews.release_all, name='lock_release_all'),
    path('lock/lock', lockviews.lock, name='lock_lock'),
    path('lock/unlock', lockviews.unlock, name='lock_unlock'),

    # crud
    path('crud/create', crudviews.create, name='crud_create'),
    path('crud/delete', crudviews.delete, name='crud_delete'),
    path('crud/update', crudviews.update, name='crud_update'),
    path('crud/query', crudviews.query, name='crud_query'),
    path('crud/queryall', crudviews.query_all, name='crud_queryall'),

    # matadata
    path('metadata/refresh',
         metadataviews.metadata_refresh,
         name='metadata_refresh'),
    path('metadata/clear',
         metadataviews.metadata_clear,
         name='metadata_clear'),
    path('metadata/save',
         metadataviews.metadata_save,
         name='metadata_save'),
    path('metadata/delete',
         metadataviews.metadata_delete,
         name='metadata_delete'),
    path('metadata/pushlive',
         metadataviews.metadata_pushlive,
         name='metadata_pushlive'),
    path('metadata/restore',
         metadataviews.metadata_restore,
         name='metadata_restore'),
    path('metadata/export',
         metadataviews.metadata_export,
         name='metadata_export'),
    path('metadata/import',
         metadataviews.metadata_import,
         name='metadata_import'),
    path('metadata/view',
         metadataviews.metadata_view,
         name='metadata_view'),

    # metadata management
    path('metadatamanagement/index',
         metadataviews.metadata_management_index,
         name='metadata_management_index'),
    path('metadatamanagement/exportbackup',
         metadataviews.metadata_management_export_backup,
         name='metadata_management_export_backup'),
    path('metadatamanagement/importbackup',
         metadataviews.metadata_management_import_backup,
         name='metadata_management_import_backup'),
    path('metadatamanagement/lockall',
         metadataviews.metadata_management_lock_all,
         name='metadata_management_lockall'),
    path('metadatamanagement/unlockall',
         metadataviews.metadata_management_unlock_all,
         name='metadata_management_unlockall'),
    path('metadatamanagement/restoreall',
         metadataviews.metadata_management_restore_all,
         name='metadata_management_restoreall'),
    path('metadatamanagement/pushliveall',
         metadataviews.metadata_management_pushlive_all,
         name='metadata_management_pushliveall'),
    path('metadatamanagement/snapshot',
         metadataviews.metadata_management_snapshot,
         name='metadata_management_snapshot'),
    path('metadatamanagement/restore',
         metadataviews.metadata_management_restore,
         name='metadata_management_restore'),
    path('metadatamanagement/pushlive',
         metadataviews.metadata_management_pushlive,
         name='metadata_management_pushlive'),
]


# global init functions
