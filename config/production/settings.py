# coding:utf-8

import os
from config.common import *

DEBUG = True

ALLOWED_HOSTS = ['*']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'standard': {
            'format': '%(asctime)s %(pathname)s %(lineno)d [%(levelname)s] %(message)s',
        },
        'simple': {
            'format': '%(asctime)s %(module)s [%(levelname)s] %(message)s',
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.abspath(__file__),
                                     '../../../log/gmt.log'),
            'maxBytes': 1024 * 1024 * 1024,  # 1G
            'backupCount': 8,
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        '': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'library': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'gmt': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}

METADATA_APP_URL = 'http://internal-antia-production-gs-local-1571031397.us-east-1.elb.amazonaws.com/meta'
METADATA_APP_NAME = 'metadata'
METADATA_APP_SECRET = 'dcdbad5686fc0096ff507a36f317245c064a8074bc5a5abd'

GMT_APP_URL = 'http://internal-antia-production-gs-local-1571031397.us-east-1.elb.amazonaws.com'
GMT_APP_NAME = 'gm'
GMT_APP_SECRET = 'odbz2veh278kidx7hkoa318nbp6for8of5x71duysrt0t45k'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'antia_gmt',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}
print('production settings loaded')

