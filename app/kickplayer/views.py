# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.kickplayer import models


@login_required
@permission_required('gpermission.kickplayer_read')
def index(request):
    m = models.get_model(1)
    ctx = {
        'kickplayer': {
            'name': m.name if m else 'kickplayer',
        },
    }
    return layout.render_with_layout(request,
                                     'kickplayer',
                                     'kickplayer_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.kickplayer_write')
@lock_required('kickplayer')
def addone(request):
    #models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])

@login_required
@permission_required('gpermission.kickplayer_write')
@lock_required('kickplayer')
def kickout(request):
    if request.method == 'POST':
        print(request.POST)
        fpid = int(request.POST['kickplayer_fpid'])
        ret = models.kickout(fpid)
        print(ret)
        return redirect("kickplayer_index")