# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.deleteuser import models


@login_required
@permission_required('gpermission.deleteuser_read')
def index(request):
    m = models.get_model(1)
    ctx = {
        'deleteuser': {
            'name': m.name if m else 'deleteuser',
        },
    }
    return layout.render_with_layout(request,
                                     'deleteuser',
                                     'deleteuser_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.deleteuser_write')
@lock_required('deleteuser')
def addone(request):
    models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])

@login_required
@permission_required('gpermission.deleteuser_write')
@lock_required('deleteuser')
def delete(request):
    if request.method == 'POST':
        print(request.POST)
        fpId = int(request.POST['fpid'])
        roleId = int(request.POST['roleid'])
        ret = models.delete_user(fpId,roleId)
        print(ret)
        return redirect("deleteuser_index")
