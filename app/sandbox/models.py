# coding: utf-8

import json
from django.core import serializers
from django.db import models


class Sandbox(models.Model):
    name = models.CharField(max_length=128, unique=True)
    hash = models.CharField(max_length=64, default='')
    level = models.CharField(max_length=64, default='')


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Sandbox.objects.all()))])
