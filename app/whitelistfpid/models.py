# coding:utf-8

import json
from django.core import serializers
from django.db import models

from django.conf import settings
from pypuzlib import httprpc as rpc


class Whitelistfpid(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Whitelistfpid.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Whitelistfpid(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Whitelistfpid.objects.all()))])


# get whitelist by fp id
def get_whitelist_by_fpid():
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('getWhitelistByFpId', [])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def mod_whitelist_fpid(fpid, op):
    req = {'fp_id': fpid, 'append_or_remove': op}
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('sendWhitelistModFpId', req)
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}
