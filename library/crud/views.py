# coding:utf-8

import json
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.utils.translation import gettext as _
from django.utils.module_loading import import_string
from library.layout import response_json, response_json_model
from library import lock as LockLib

CRUD = import_string(settings.CRUD_CONFIG) if \
    hasattr(settings, 'CRUD_CONFIG') and settings.CRUD_CONFIG \
    else {}


@login_required
def create(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        crud = CRUD[data['crud']]
        perm = crud['permission']
        if not request.user.has_perm(perm):
            return response_json(code=-1,
                                 message=_('do not have perm'))
        lockname = crud['lock']
        if not LockLib.has_lock(request.user.username, lockname):
            return response_json(code=-1,
                                 message=_('do not have lock'))

        modelclass = import_string(crud['model'])
        if 'id' in data['fields']:
            if modelclass.objects.filter(id=data['fields']['id']).exists():
                return response_json(code=-1,
                                     message=_('id already exists'))

        modelclass(**(data['fields'])).save()

        # success
        return response_json()
    return response_json(code=-1,
                         message=_('only support post method'))


@login_required
def delete(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        crud = CRUD[data['crud']]
        perm = crud['permission']
        if not request.user.has_perm(perm):
            return response_json(code=-1,
                                 message=_('do not have perm'))
        lockname = crud['lock']
        if not LockLib.has_lock(request.user.username, lockname):
            return response_json(code=-1,
                                 message=_('do not have lock'))

        modelclass = import_string(crud['model'])
        modelclass.objects.filter(id=data['id']).delete()

        # success
        return response_json()
    return response_json(code=-1,
                         message=_('only support post method'))


@login_required
def update(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        crud = CRUD[data['crud']]
        perm = crud['permission']
        if not request.user.has_perm(perm):
            return response_json(code=-1,
                                 message=_('do not have perm'))
        lockname = crud['lock']
        if not LockLib.has_lock(request.user.username, lockname):
            return response_json(code=-1,
                                 message=_('do not have lock'))

        modelclass = import_string(crud['model'])
        qs = modelclass.objects.filter(id=data['id'])
        if not qs.exists():
            return response_json(code=-1,
                                 message=_('id does not exist'))

        qs.update(**(data['fields']))
        return response_json()

    return response_json(code=-1,
                         message=_('only support post method'))


@login_required
def query(request):
    crud = CRUD[request.GET['crud']]
    perm = crud['permission']
    if not request.user.has_perm(perm):
        return response_json(code=-1,
                             message=_('do not have perm'))

    modelclass = import_string(crud['model'])
    data = modelclass.objects.filter(id=request.GET['id'])
    return response_json_model(data)


@login_required
def query_all(request):
    crud = CRUD[request.GET['crud']]
    perm = crud['permission']
    if not request.user.has_perm(perm):
        return response_json(code=-1,
                             message=_('do not have perm'))

    modelclass = import_string(crud['model'])
    data = modelclass.objects.all()
    return response_json_model(data)
