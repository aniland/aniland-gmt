# coding:utf-8
from datetime import datetime
import time

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.sealfpid import models


@login_required
@permission_required('gpermission.sealfpid_read')
def index(request):
    resp = models.get_seal_fpid()

    # build list
    ctx = {'dataList': []}

    if resp['fp_id_list']:
        for i, v in enumerate(resp['fp_id_list']):
            # print(v)
            strTime = models.timestampToTime(v['expired_time'] / 1000)
            ctx['dataList'].append({'id': v['id'], 'expired_time': strTime})

    return layout.render_with_layout(request,
                                     'sealfpid',
                                     'sealfpid_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.sealfpid_write')
@lock_required('sealfpid')
def addone(request):
    if request.method == 'POST':
        print(request.POST)

        fpId = int(request.POST['sealFpId'])
        expiredTime = request.POST['expiredTime']

        timeArray = datetime.strptime(expiredTime, '%Y-%m-%d %H:%M:%S').utctimetuple()

        # make time
        timestamp = int(time.mktime(timeArray)) * 1000
        # print(timestamp)

        resp = models.mod_seal_fpid(fpId, 1, timestamp)

        # build list
        ctx = {'dataList': []}

        if resp['fp_id_list']:
            for i,v in enumerate(resp['fp_id_list']):
                 #print(v)
                strTime = models.timestampToTime(v['expired_time']/1000)
                ctx['dataList'].append({'id':v['id'],'expired_time':strTime})

        return layout.render_with_layout(request,
                                         'sealfpid',
                                         'sealfpid_index.html',
                                         ctx)


@login_required
@permission_required('gpermission.sealfpid_write')
@lock_required('sealfpid')
def delete(request):
    if request.method == 'POST':
        print(request.POST)

        fpId = int(request.POST['fpid'])

        resp = models.mod_seal_fpid(fpId, 2, 0)

        # build list
        ctx = {'dataList': []}

        if resp['fp_id_list']:
            for i,v in enumerate(resp['fp_id_list']):
                 #print(v)
                strTime = models.timestampToTime(v['expired_time']/1000)
                ctx['dataList'].append({'id':v['id'],'expired_time':strTime})

        return layout.render_with_layout(request,
                                         'sealfpid',
                                         'sealfpid_index.html',
                                         ctx)
