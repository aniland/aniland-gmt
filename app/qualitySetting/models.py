# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Qualitysetting(models.Model):
    dname = models.CharField(max_length=128)
    pt = models.CharField(max_length=128, default="")
    num = models.IntegerField(default=0)
    pc = models.IntegerField(default=0)
    gsl = models.IntegerField(default=0)
    gdname = models.CharField(max_length=128, default="")
    os = models.CharField(max_length=128, default="")
    config_id = models.IntegerField(default=1)
    white_list = models.BooleanField(default=False)
    class Meta:
        unique_together = ['dname', 'pt']

class QualitysettingConf(models.Model):
    config_name = models.CharField(max_length=128, default="")
    shader_lod = models.IntegerField(default=200)
    antialiasing = models.IntegerField(default=0)
    show_particle_effect = models.BooleanField(default=True)
    screen_resolution = models.FloatField(default=0.8)
    graphics_tier = models.IntegerField(default=0)
    particle_effect_level = models.IntegerField(default=1)
    max_screen_resolution  = models.IntegerField(default=1344)
    show_screen_effect = models.BooleanField(default=False)
    quality_level = models.IntegerField(default=1)

def get_model(id):
    try:
        return Qualitysetting.objects.get(id=id)
    except Exception:
        return {}


def metadata_process():
    confData = json.loads(serializers.serialize('json',QualitysettingConf.objects.all()))
    confValues = []

    for d in confData:
        value = d.get('fields', {})
        if 'id' not in value and 'pk' in d:
            value['id'] = d['pk']
        confValues.append(value)

    return json.dumps({'conf':confValues,
                       'device':[p.get('fields', {}) for p in json.loads(serializers.serialize('json',Qualitysetting.objects.all()))]})
