# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.qualitySetting import models


@login_required
@permission_required('gpermission.qualitySetting_read')
def index(request):
    sd = list(models.Qualitysetting.objects.all())
    qsf = list(models.QualitysettingConf.objects.all())
    # print(sd)
    ctx = {
        'qualitySetting': sd,
        'qualitySettingConf' :qsf
    }
    return layout.render_with_layout(request,
                                     'qualitySetting',
                                     'qualitySetting_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.qualitySetting_write')
@lock_required('qualitySetting')
def addone(request):
    models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])
