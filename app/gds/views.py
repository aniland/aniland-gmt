# coding: utf-8

from django.contrib.auth.decorators import login_required, permission_required
from library import layout


@login_required
@permission_required('gpermission.gds_read')
def index(request):
    return layout.render_with_layout(request, 'gds', 'gds_index.html')
