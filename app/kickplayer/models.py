# coding:utf-8

import json
from django.core import serializers
from django.db import models


class Kickplayer(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Kickplayer.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Kickplayer(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Kickplayer.objects.all()))])


def kickout(fpid):
    from django.conf import settings
    from pypuzlib import httprpc as rpc
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('kick-player', {'fpid':fpid})
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}
