# coding:utf-8
import json
import time
from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import (
    login_required, permission_required
)
from pypuzlib import httprpc as rpc

from app.mail import models
from library import layout


@login_required
@permission_required('gpermission.mail_read')
def index(request):
    m = models.get_model(1)
    ctx = {
        'mail': {
            'name': m.name if m else 'mail',
        },
    }
    return layout.render_with_layout(request,
                                     'mail',
                                     'mail_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.mail_write')
def addone(request):
    if request.method == 'POST':
        mail_str = json.dumps(request.POST)
        mail = json.loads(mail_str)
        try:
            if len(mail['title']) <= 0:
                raise Exception(request, "input title cannot be empty")

            if len(mail['content']) <= 0:
                raise Exception(request, "input content cannot be empty")

            mail['mail_type'] = int(mail['mail_type'])
            # person mail
            if mail['mail_type'] == 101:
                if len(mail['role_ids']) <= 0:
                    raise Exception("roleIds for player mail cannot be empty")
                mail['role_ids'] = [int(i) for i in mail['role_ids'].strip().split(',')]
                if len(mail['role_ids']) != len(set(mail['role_ids'])):
                    raise Exception("duplicated roleIds", mail['role_ids'])
            # global mail
            elif mail['mail_type'] == 102:
                if len(mail['role_ids']) > 0:
                    raise Exception("roleIds for global mail must be empty")
                mail['role_ids'] = []
            else:
                raise Exception("mail_type error", mail['mail_type'])

            item_ids = request.POST.getlist('item_id', [])
            counts = request.POST.getlist('count', [])
            attachment = []
            for i, item_id in enumerate(item_ids):
                count = counts[i]
                if item_id == '' and count == '':
                    continue
                if int(item_id) <= 0:
                    raise Exception("itemId should be larger than 0", item_id)
                if int(count) <= 0:
                    raise Exception("item count should be larger than 0", count)
                attachment.append({"item_id": int(item_id), "count": int(count)})
            mail['attachment'] = json.dumps(attachment)

            if mail['create'] == "":
                mail['created_time'] = 0
            else:
                create_time = datetime.strptime(mail['create'], '%Y-%m-%d %H:%M:%S %z').utctimetuple()
                mail['created_time'] = int(time.mktime(create_time))
            if mail['expire'] == "":
                mail['expired_time'] = 0
            else:
                expired = datetime.strptime(mail['expire'], '%Y-%m-%d %H:%M:%S %z').utctimetuple()
                mail['expired_time'] = int(time.mktime(expired))
        except Exception as e:
            messages.error(request, e)
            return layout.render_with_layout(request, 'mail', 'mail_index.html', {'err': 'Input error!'})

        print("mail:%s" % mail)

        c, m, p = rpc.clients[settings.GMT_APP_NAME].request('sendGMMail', mail)
        if c != 0:
            messages.error(request, m)
        return layout.render_with_layout(request, 'mail', 'mail_index.html', {'p': p})
