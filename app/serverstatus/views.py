# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect

from app.serverstatus import models
from library import layout
from library.lock import lock_required
from app.serverstatus.models import Serverstatus
import datetime


@login_required
@permission_required('gpermission.serverstatus_read')
def index(request):
    data_list = list(Serverstatus.objects.all().order_by('-id'))
    ctx = {'serverstatus': {}}

    if data_list:
        ctx['serverstatus'] = {
            'id': data_list[0].id,
            'open_or_close': data_list[0].open_or_close,
            'effective_time': data_list[0].effective_time
        }
    else:
        ctx['serverstatus'] = {
            'id': 0,
            'open_or_close': 1,
            'effective_time': "2019-01-01 00:00:00"
        }
    return layout.render_with_layout(request,
                                     'serverstatus',
                                     'serverstatus_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.serverstatus_write')
@lock_required('serverstatus')
def addone(request):
    if request.method == 'POST':
        print(request.POST)
        effective_time_v = request.POST['effective_time']

        # TODO 加入时间检测
        models.timeToTimestamp(effective_time_v)

        obj = Serverstatus(open_or_close=request.POST['open_or_close'],
                           effective_time=effective_time_v)
        obj.save()
        return redirect('serverstatus_index')
