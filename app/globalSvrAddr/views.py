# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.globalSvrAddr import models


@login_required
@permission_required('gpermission.globalSvrAddr_read')
def index(request):
    resp = models.get_all_svr_addr()
    ctx = {'dataList': resp}
    return layout.render_with_layout(request,
                                     'globalSvrAddr',
                                     'globalSvrAddr_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.globalSvrAddr_write')
@lock_required('globalSvrAddr')
def addone(request):
    if request.method == 'POST':
        # print(request.POST)

        _ = models.add_svr_addr(int(request.POST['gameId']),
                                    request.POST['environment'],
                                    request.POST['svraddr'])
        return redirect('globalSvrAddr_index')


@login_required
@permission_required('gpermission.globalSvrAddr_write')
@lock_required('globalSvrAddr')
def delete(request):
    if request.method == 'POST':
        # print(request.POST)

        _ = models.del_svr_addr(int(request.POST['gameId']),
                                    request.POST['environment'])
        return redirect('globalSvrAddr_index')
