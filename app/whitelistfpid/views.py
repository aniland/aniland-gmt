# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect

from app.whitelistfpid import models
from app.whitelistip.views import WhitelistOp
from library import layout
from library.lock import lock_required
from app.whitelistip import views
from time import time

@login_required
@permission_required('gpermission.whitelistfpid_read')
def index(request):
    resp = models.get_whitelist_by_fpid()

    ctx = {}
    ctx['dataList'] = resp

    return layout.render_with_layout(request,
                                     'whitelistfpid',
                                     'whitelistfpid_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.whitelistfpid_write')
@lock_required('whitelistfpid')
def addone(request):
    if request.method == 'POST':
        print(request.POST)

        fpId = int(request.POST['FpId'])

        resp = models.mod_whitelist_fpid(fpId, WhitelistOp.APPEND.value)

        ctx = {'dataList': resp}

        return layout.render_with_layout(request,
                                         'whitelistfpid',
                                         'whitelistfpid_index.html',
                                         ctx)

@login_required
@permission_required('gpermission.whitelistfpid_write')
@lock_required('whitelistfpid')
def delete(request):
    if request.method == 'POST':
        print(request.POST)

        fpId = int(request.POST['FpId'])

        resp = models.mod_whitelist_fpid(fpId, WhitelistOp.REMOVE.value)

        ctx = {'dataList': resp}

        return layout.render_with_layout(request,
                                     'whitelistfpid',
                                     'whitelistfpid_index.html',
                                     ctx)