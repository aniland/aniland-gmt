# coding: utf-8

import os
import json
import hashlib
import logging
from django.db import models
from django.conf import settings
from django.utils.module_loading import import_string
from django.core import serializers
from pypuzlib import httprpc as rpc

METADATA = import_string(settings.METADATA_CONFIG) if \
    hasattr(settings, 'METADATA_CONFIG') and settings.METADATA_CONFIG \
    else {}

TMP_DIR = os.path.join(getattr(settings, 'TMP_DIR', '/tmp'), 'metadata')

logger = logging.getLogger('library.metadata')


def _init_rpc():
    if settings.METADATA_APP_URL and \
       settings.METADATA_APP_NAME not in rpc.clients:
        rpc.add_client(settings.METADATA_APP_NAME,
                       settings.METADATA_APP_SECRET,
                       settings.METADATA_APP_URL.rstrip('/') + '/metadata')


class Metadata(models.Model):
    name = models.CharField(max_length=128)
    hash = models.CharField(max_length=128)
    live = models.BooleanField(default=False)
    time = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=256)
    size = models.IntegerField()
    comment = models.CharField(max_length=2048)

    class Meta:
        unique_together = ['name', 'hash']


class MetadataSource(models.Model):
    owner = models.ForeignKey(Metadata, on_delete=models.CASCADE)
    content = models.TextField(max_length=1024*1024*16)


class MetadataProcessed(models.Model):
    owner = models.ForeignKey(Metadata, on_delete=models.CASCADE)
    content = models.TextField(max_length=1024*1024*16)


def get_metadata_source(id):
    return MetadataSource.objects.get(id=id)


def get_metadata_processed(id):
    return MetadataProcessed.objects.get(id=id)


def get_metadata(id):
    return Metadata.objects.get(id=id)


def get_metadata_list(name):
    try:
        return Metadata.objects.filter(name=name)
    except Metadata.DoesNotExist:
        return []


def delete_metadata(id):
    # source and processed will be deleted automatically
    Metadata.objects.filter(id=id).delete()


def save_metadata(name, author, comment):
    source = generate_metadata_source(name)
    processed = generate_metadata_processed(name)
    hash = generate_metadata_hash(processed)

    try:
        md = Metadata.objects.get(name=name, hash=hash)
    except Metadata.DoesNotExist:
        md = None

    # already exists
    if md:
        return md.id

    md = Metadata(name=name,
                  live=False,
                  hash=hash,
                  author=author,
                  size=len(processed),
                  comment='[{}]{}'.format(settings.ENVIRONMENT, comment))
    md.save()

    mdsource = MetadataSource(owner=md, content=source)
    mdsource.save()

    mdprocessed = MetadataProcessed(owner=md, content=processed)
    mdprocessed.save()

    return md.id


def get_live_metadata(name):
    # load from local storage
    if not getattr(settings, 'METADATA_APP_URL', ''):
        try:
            md = Metadata.objects.get(name=name, live=True)
            processed = MetadataProcessed.objects.get(owner=md)
            return {'hash': md.hash, 'content': processed.content}
        except Metadata.DoesNotExist:
            return {'hash': '', 'content': ''}

    # pull from remote storage
    _init_rpc()
    c, m, p = rpc.clients[settings.METADATA_APP_NAME].request('pull', [name])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))

    if not p:
        return {'hash': '', 'content': ''}
    else:
        return p[0]


def pushlive_metadata(name, id):
    # save to local storage
    if not getattr(settings, 'METADATA_APP_URL', ''):
        try:
            mdcur = Metadata.objects.get(name=name, live=True)
            mdcur.live = False
            mdcur.save()
        except Metadata.DoesNotExist:
            # no local storage, do nothing
            pass

        md = get_metadata(id)
        md.live = True
        md.save()
        return

    # push to remote storage
    _init_rpc()
    md = Metadata.objects.get(name=name, id=id)
    processed = MetadataProcessed.objects.get(owner=md)
    c, m, p = rpc.clients[settings.METADATA_APP_NAME].request('push', [{
        'name': name,
        'hash': md.hash,
        'content': processed.content
    }])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))


def clear_metadata_source(name):
    if 'clear' in METADATA[name]:
        clear = import_string(METADATA[name]['clear'])
        clear()
        return

    # iterator from end to start, for handling dependency
    for m in reversed(METADATA[name]['models']):
        import_string(m).objects.all().delete()


def restore_metadata_from_source(name, source):
    if not source:
        return

    if 'restore' in METADATA[name]:
        restore = import_string(METADATA[name]['restore'])
        restore(json.loads(source))
        return

    data = json.loads(source)
    for d in data:
        if d['name'] not in METADATA[name]['models']:
            continue

        for obj in serializers.deserialize("json", d['payload']):
            obj.save()


def generate_metadata_source(name):
    if 'source' in METADATA[name]:
        source = import_string(METADATA[name]['source'])
        v = source()
        if isinstance(v, (list, tuple, dict)):
            return json.dumps(v)
        elif isinstance(v, str):
            return v
        else:
            raise Exception('illeagal source value type:{}'.format(type(v)))

    md = []
    for m in METADATA[name]['models']:
        value = import_string(m).objects.all()
        payload = serializers.serialize('json', value)
        md.append({'name': m, 'payload': payload})
    return json.dumps(md)


def generate_metadata_processed(name):
    if 'process' in METADATA[name]:
        process = import_string(METADATA[name]['process'])
        v = process()
        if isinstance(v, (list, tuple, dict)):
            return json.dumps(v)
        elif isinstance(v, str):
            return v
        else:
            raise Exception('illeagal processed value type:{}'.format(type(v)))

    md = []
    for m in METADATA[name]['models']:
        payload = import_string(m).objects.all()
        md.append({'name': m, 'payload': payload})
    return json.dumps(md)


def generate_metadata_hash(content):
    m = hashlib.md5()
    m.update(content.encode('utf-8'))
    return str(m.hexdigest())


def get_live_metadata_infos(names):
    # load from local storage
    if not getattr(settings, 'METADATA_APP_URL', ''):
        try:
            result = {}
            mds = Metadata.objects.filter(live=True)
            for md in mds:
                if md.name not in names:
                    continue

                result[md.name] = {
                    'name': md.name,
                    'hash': md.hash,
                    'author': md.author,
                    'time': md.time,
                    'comment': md.comment,
                }
            return result
        except Metadata.DoesNotExist:
            return {}

    # pull from remote storage
    _init_rpc()
    c, m, p = rpc.clients[settings.METADATA_APP_NAME].request('info', names)
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    if not p:
        return {}

    result = {}
    for info in p:
        if info['name'] not in names:
            continue

        try:
            md = Metadata.objects.get(name=info['name'], hash=info['hash'])
        except Exception:
            result[info.name] = {
                'name': info['name'],
                'hash': info['hash'],
            }
        else:
            result[info['name']] = {
                'name': md.name,
                'hash': md.hash,
                'time': md.time,
                'author': md.author,
                'comment': md.comment,
            }
    return result


def get_current_metadata_infos(names):
    result = {}
    for name in names:
        value = generate_metadata_processed(name)
        hash = generate_metadata_hash(value)
        try:
            md = Metadata.objects.get(name=name, hash=hash)
        except Exception:
            result[name] = {
                'name': name,
                'hash': hash,
            }
        else:
            result[name] = {
                'name': name,
                'hash': hash,
                'time': md.time,
                'author': md.author,
                'comment': md.comment,
            }
    return result
