# coding:utf-8

from copy import deepcopy
from django.utils.translation import gettext_lazy as _


MODULES = [
    {
        'name': 'development',
        'title': _('Development'),
        'icon': 'fas fa-keyboard',
        'module': [
            {
                'name': 'metadata_management',
                'title': _('Metadata'),
                'icon': 'fas fa-yin-yang',
                'url': 'metadata_management_index',
                'permission': 'gpermission.metadata_management_read',
                'lock': 'metadata_management',
            },
        ]
    },
    {
        'name': 'account',
        'title': _('Account'),
        'icon': 'fas fa-user',
        'module': [
            {
                'name': 'account_user',
                'title': _('User'),
                'icon': 'fas fa-book-reader',
                'url': 'account_user_index',
                'permission': 'gpermission.account_user_read',
                'lock': 'account_user',
            },
            {
                'name': 'account_group',
                'title': _('Group'),
                'icon': 'fas fa-link',
                'url': 'account_group_index',
                'permission': 'gpermission.account_group_read',
                'lock': 'account_group',
            },
            {
                'name': 'lock',
                'title': _('Locks'),
                'icon': 'fas fa-lock',
                'url': 'lock_index',
                'permission': 'gpermission.lock_read',
            },
        ]
    },
]


def merge_modules(left, right):
    result = []
    for itemleft in left:
        name = itemleft['name']
        itemright = _get_named_item(name, right)
        if not itemright:
            result.append(deepcopy(itemleft))
            continue

    for itemleft in left:
        name = itemleft['name']
        r = _get_named_item(name, result)
        if r:
            # already in result, skip it
            continue
        itemright = _get_named_item(name, right)

        itemresult = {'name': name}
        _assign_value('title', itemresult, itemleft, itemright, '')
        _assign_value('icon', itemresult, itemleft, itemright, '')

        if 'module' in itemleft or 'module' in itemresult:
            itemresult['module'] = (itemleft.get('module', []) +
                                    itemright.get('module', []))

        if 'group' in itemleft or 'group' in itemright:
            itemresult['group'] = merge_modules(itemleft.get('group', []),
                                                itemright.get('group', []))

        result.append(itemresult)

    for itemright in right:
        name = itemright['name']
        itemleft = _get_named_item(name, left)
        if not itemleft:
            result.append(deepcopy(itemright))

    return result


def _assign_value(name, item, left, right, default):
    item[name] = left.get(name, right.get(name, default))


def _get_named_item(name, items):
    for item in items:
        if item['name'] == name:
            return item
    return {}
