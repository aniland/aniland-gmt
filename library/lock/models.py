# coding: utf-8

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils.module_loading import import_string


LOCKS = import_string(settings.LOCK_CONFIG) if \
    hasattr(settings, 'LOCK_CONFIG') and settings.LOCK_CONFIG \
    else []


def get_lock_config(name):
    for l in LOCKS:
        if l[0] == name:
            return l
    return None


class Lock(models.Model):
    name = models.CharField(max_length=128, unique=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    locked_by = models.CharField(max_length=128)
    locked_time = models.DateTimeField(auto_now=True)
    lock_title = models.CharField(max_length=256, default='')


def lock(lockname, user):
    try:
        lck = Lock.objects.get(name=lockname)
    except Lock.DoesNotExist:
        lck = None

    if lck and lck.locked_by == user.username:
        return True, ""
    if lck and lck.locked_by and lck.locked_by != user.username:
        return False, '{} locked by {}'.format(lck.name, lck.locked_by)

    if not lck:
        lck = Lock(name=lockname,
                   owner=user,
                   locked_by=user.username)
    else:
        lck.owner = user
        lck.locked_by = user.username

    item = get_lock_config(lockname)
    if user.has_perm(item[1]):
        lck.lock_title = item[2]
        lck.save()
        return True, ""
    else:
        return False, 'no permission to lock {}'.format(lockname)


def unlock(lockname, user):
    Lock.objects.filter(name=lockname,
                        locked_by=user.username).delete()
    return True, ""
