# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.lock import LOCKS as LIBLOCKS

LOCKS = LIBLOCKS + [
    (
        'sandbox',
        'gpermission.sandbox_write',
        _('Sandbox'),
    ),
    (
        'gds',
        'gpermission.gds_write',
        _('GDS')
    ),
    (
        'event',
        'gpermission.event_write',
        _('Event')
    ),
    (
        'userinfo',
        'gpermission.userinfo_write',
        _('User Info')
    ),
    (
        'user_probe',
        'gpermission.userprobe_write',
        _('User Probe'),
    ),
    (
        'plog',
        'gpermission.plog_write',
        _('User PLog Setting'),
    ),

    (
        'mail',
        'gpermission.mail_write',
        _('Mail'),
    ),

    (
        'qualitySetting',
        'gpermission.qualitySetting_write',
        _('Qualitysetting'),
    ),

    (
        'whitelistfpid',
        'gpermission.whitelistfpid_write',
        _('Whitelistfpid'),
    ),

    (
        'whitelistip',
        'gpermission.whitelistip_write',
        _('Whitelistip'),
    ),

    (
        'sealfpid',
        'gpermission.sealfpid_write',
        _('Sealfpid'),
    ),

    (
        'serverstatus',
        'gpermission.serverstatus_write',
        _('Serverstatus'),
    ),

    (
        'globalSvrAddr',
        'gpermission.globalSvrAddr_write',
        _('Globalsvraddr'),
    ),

    (
        'globalRoutingRules',
        'gpermission.globalRoutingRules_write',
        _('Globalroutingrules'),
    ),

    (
        'kickplayer',
        'gpermission.kickplayer_write',
        _('Kickplayer'),
    ),

    (
        'deleteuser',
        'gpermission.deleteuser_write',
        _('Deleteuser'),
    ),

    (
        'rolecopy',
        'gpermission.rolecopy_write',
        _('Rolecopy'),
    ),

    (
        'GmtTest',
        'gpermission.GmtTest_write',
        _('Gmttest'),
    ),
]
