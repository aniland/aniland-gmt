# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.globalRoutingRules import models


@login_required
@permission_required('gpermission.globalRoutingRules_read')
def index(request):
    resp = models.get_all_routing_rules()
    ctx = {'dataList': resp}
    return layout.render_with_layout(request,
                                     'globalRoutingRules',
                                     'globalRoutingRules_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.globalRoutingRules_write')
@lock_required('globalRoutingRules')
def addone(request):
    if request.method == 'POST':
        # print(request.POST)

        _ = models.add_routing_rules(int(request.POST['gameId']),
                                     request.POST['gameversion'],
                                     request.POST['environment'])
        return redirect('globalRoutingRules_index')


@login_required
@permission_required('gpermission.globalRoutingRules_write')
@lock_required('globalRoutingRules')
def delete(request):
    if request.method == 'POST':
        # print(request.POST)

        _ = models.del_routing_rules(int(request.POST['gameId']),
                                     request.POST['gameversion'])

        return redirect('globalRoutingRules_index')
