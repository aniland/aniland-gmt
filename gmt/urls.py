"""gmt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from app.customservice import views as customservice_tviews
from app.event import views as eventviews
from app.gds import views as gdsviews
from app.mail import views as mail_views
from app.sandbox import views as sandboxviews
from app.userprobe import views as userprobe_views
from app.qualitySetting import views as qualitySetting_views
from app.whitelistfpid import views as whitelistfpid_views
from app.whitelistip import views as whitelistip_views
from app.sealfpid import views as sealfpid_views
from app.serverstatus import views as serverstatus_views
from app.globalSvrAddr import views as globalSvrAddr_views
from app.globalRoutingRules import views as globalRoutingRules_views
from app.kickplayer import views as kickplayer_views
from app.deleteuser import views as deleteuser_views
from app.rolecopy import views as rolecopy_views
from app.GmtTest import views as GmtTest_views
from library.config.urls import urlpatterns as library_urlpatterns

urlpatterns = library_urlpatterns + [
    # sandbox
    path('development/sandbox/index',
         sandboxviews.index,
         name='sandbox_index'),
    path('development/sandbox/save',
         sandboxviews.save,
         name='sandbox_save'),

    # game
    path('game/gds/index', gdsviews.index, name='gds_index'),

    # operation
    path('operation/event/index', eventviews.index, name='event_index'),

    # custom service
    path('customservice/userinfo/index',
         customservice_tviews.userinfo_index,
         name='userinfo_index'),
    path('customservice/plog/index',
         customservice_tviews.plog_index,
         name='plog_index'),
    # mail
    path('mail/index',
         mail_views.index,
         name='mail_index'),
    # mail
    path('mail/addone',
         mail_views.addone,
         name='mail_addone'),
    # testing
    path('testing/userprobe/index',
         userprobe_views.index,
         name='userprobe_index'),

    # qualitySetting
    path('qualitySetting/index',
         qualitySetting_views.index,
         name='qualitySetting_index'),
    path('qualitySetting/addone',
         qualitySetting_views.addone,
         name='qualitySetting_addone'),

    # whitelistip
    path('whitelistip/index',
         whitelistip_views.index,
         name='whitelistip_index'),
    path('whitelistip/addone',
         whitelistip_views.addone,
         name='whitelistip_addone'),
    path('whitelistip/delete',
         whitelistip_views.delete,
         name='whitelistip_delete'),


    # whitelistfpid
    path('whitelistfpid/index',
         whitelistfpid_views.index,
         name='whitelistfpid_index'),
    path('whitelistfpid/addone',
         whitelistfpid_views.addone,
         name='whitelistfpid_addone'),
    path('whitelistfpid/delete',
         whitelistfpid_views.delete,
         name='whitelistfpid_delete'),

    # sealfpid
    path('sealfpid/index',
         sealfpid_views.index,
         name='sealfpid_index'),
    path('sealfpid/addone',
         sealfpid_views.addone,
         name='sealfpid_addone'),
    path('sealfpid/delete',
         sealfpid_views.delete,
         name='sealfpid_delete'),

    # serverstatus
    path('serverstatus/index',
         serverstatus_views.index,
         name='serverstatus_index'),
    path('serverstatus/addone',
         serverstatus_views.addone,
         name='serverstatus_addone'),

    # globalSvrAddr
    path('globalSvrAddr/index',
         globalSvrAddr_views.index,
         name='globalSvrAddr_index'),
    path('globalSvrAddr/addone',
         globalSvrAddr_views.addone,
         name='globalSvrAddr_addone'),
    path('globalSvrAddr/delete',
         globalSvrAddr_views.delete,
         name='globalSvrAddr_delete'),

    # globalRoutingRules
    path('globalRoutingRules/index',
         globalRoutingRules_views.index,
         name='globalRoutingRules_index'),
    path('globalRoutingRules/addone',
         globalRoutingRules_views.addone,
         name='globalRoutingRules_addone'),
    path('globalRoutingRules/delete',
         globalRoutingRules_views.delete,
         name='globalRoutingRules_delete'),

    # kickplayer
    path('kickplayer/index',
         kickplayer_views.index,
         name='kickplayer_index'),
    path('kickplayer/addone',
         kickplayer_views.addone,
         name='kickplayer_addone'),
    path('kickplayer/kickout',
         kickplayer_views.kickout,
         name='kickplayer_kickout'),

    # deleteuser
    path('deleteuser/index',
         deleteuser_views.index,
         name='deleteuser_index'),
    path('deleteuser/addone',
         deleteuser_views.addone,
         name='deleteuser_addone'),
    path('deleteuser/delete',
         deleteuser_views.delete,
         name='deleteuser_delete'),

    # rolecopy
    path('rolecopy/index',
         rolecopy_views.index,
         name='rolecopy_index'),
    path('rolecopy/addone',
         rolecopy_views.addone,
         name='rolecopy_addone'),
    path('rolecopy/get_role_data',
         rolecopy_views.get_role_data,
         name='rolecopy_get_role_data'),
    path('rolecopy/import_role_data',
         rolecopy_views.import_role_data,
         name='rolecopy_import_role_data'),
    path('rolecopy/delete',
         rolecopy_views.delete,
         name='rolecopy_delete'),

    # GmtTest
    path('GmtTest/index',
         GmtTest_views.index,
         name='GmtTest_index'),
    path('GmtTest/addone',
         GmtTest_views.addone,
         name='GmtTest_addone'),
]
