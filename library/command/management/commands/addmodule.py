# coding:utf-8

import os
import traceback
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'add a new module'

    def add_arguments(self, parser):
        parser.add_argument('name', nargs='+', type=str, help='module name')

    def handle(self, *args, **options):
        name = options['name'][0]
        try:
            path = self.create_path(name)

            self.create_init(name, path)
            self.create_apps(name, path)
            self.create_models(name, path)
            self.create_views(name, path)
            self.create_templates(name, path)
            self.create_tests(name, path)

            basedir = os.path.dirname(os.path.abspath(__file__))
            path = os.path.join(basedir, '../../../../gmt')

            self.append_permission(name, path)
            self.append_lock(name, path)
            self.append_crud(name, path)
            self.append_metadata(name, path)
            self.append_url(name, path)
            self.append_module(name, path)
        except Exception as ex:
            msg = '{}\n{}'.format(str(ex),
                                  traceback.format_exc())
            self.stdout.write(self.style.ERROR(msg))
        else:
            self.stdout.write(self.style.SUCCESS('add module success'))

    def create_path(self, name):
        self.stdout.write(self.style.SUCCESS('create app path...'))
        basedir = os.path.dirname(os.path.abspath(__file__))
        p = os.path.join(basedir, '../../../../app', name)
        if os.path.exists(p):
            raise Exception('path already exists')
        os.makedirs(p)
        return p

    def create_init(self, name, path):
        self.stdout.write(self.style.SUCCESS('create __init__.py...'))
        with open(os.path.join(path, '__init__.py'), 'w', encoding='utf-8') as f:
            f.writelines(['# coding:utf-8\n'])

    def create_apps(self, name, path):
        self.stdout.write(self.style.SUCCESS('create apps.py...'))
        with open(os.path.join(path, 'apps.py'), 'w', encoding='utf-8') as f:
            f.writelines(['# coding:utf-8\n'])

    def create_models(self, name, path):
        self.stdout.write(self.style.SUCCESS('create models.py...'))
        with open(os.path.join(path, 'models.py'), 'w', encoding='utf-8') as f:
            f.write(
"""# coding:utf-8

import json
from django.core import serializers
from django.db import models


class %(name)s(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return %(name)s.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = %(name)s(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              %(name)s.objects.all()))])
""" % {'name': name.capitalize()}
            )

    def create_views(self, name, path):
        self.stdout.write(self.style.SUCCESS('create views.py...'))
        with open(os.path.join(path, 'views.py'), 'w', encoding='utf-8') as f:
            f.write(
"""# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.%(name)s import models


@login_required
@permission_required('gpermission.%(name)s_read')
def index(request):
    m = models.get_model(1)
    ctx = {
        '%(name)s': {
            'name': m.name if m else '%(name)s',
        },
    }
    return layout.render_with_layout(request,
                                     '%(name)s',
                                     '%(name)s_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.%(name)s_write')
@lock_required('%(name)s')
def addone(request):
    models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])
""" % {'name': name}
            )

    def create_templates(self, name, path):
        self.stdout.write(self.style.SUCCESS('create templates...'))
        os.makedirs(os.path.join(path, 'templates'))

        with open(os.path.join(path,
                               'templates/' + name + '_index.html'),
                  'w', encoding='utf-8') as f:
            f.write(
"""{%% extends "page_layout.html" %%}

{%% load i18n %%}

{%% block content %%}

<div id="%(name)s_index">
  <h5>{%% trans 'Welcome to' %%}: {{ %(name)s.name }}</h5>
  <button type="button"
    id="btn-create"
    class="btn btn-primary"
    crud-module="%(name)s"
    onclick="addOne()"
    {{ lock.status_attr }}>
    CRUD Test
  </button>
  <form action={%% url '%(name)s_addone' %%} method="post">
    {%% csrf_token %%}
    <input type="text"
      name="name"
      value="test him"
      style="display:none">
    <input type="text"
      name="redirect"
      value="{{ request.get_full_path }}"
      style="display:none">
    <button type="submit"
      class="btn btn-danger"
      {{ lock.status_attr}}>
      {%% trans 'Form Test' %%}
    </button>
  </form>
  <a href="#"
    data-type="text"
    data-pk="1"
    data-crud="%(name)s"
    data-name="name"
    data-value="{{ %(name)s.name }}"
    data-disabled="{{ lock.status_attr }}">
  </a>
  <script>
    function addOne() {
      L.crudCreate('btn-create', {
        name: 'test me',
      }, function() {
        L.info('create data success')
      })
    }

    $(document).ready(function() {
      //init functions when page is ready
      $('a[data-crud]').each(function(idx, elem) {
        L.editable($(elem));
      });
    })
  </script>
</div>

{%% endblock %%}
""" % {'name': name}
            )

    def create_tests(self, name, path):
        self.stdout.write(self.style.SUCCESS('create tests.py...'))
        with open(os.path.join(path, 'tests.py'), 'w', encoding='utf-8') as f:
            f.writelines(['# coding:utf-8\n'])

    def append_permission(self, name, path):
        self.stdout.write(self.style.SUCCESS('append permission.py...'))

        lines = []
        with open(os.path.join(path, 'permission.py'), encoding='utf-8') as f:
            lines = f.readlines()
        if not lines:
            lines.append(
"""# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.permission import PERMISSIONS as LIBPERMISSIONS


PERMISSIONS = LIBPERMISSIONS + [
]
""")

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == ']':
                lines.insert(idx,
"""
    ('{name}_read', _('{title} Read')),
    ('{name}_write', _('{title} Write')),
""".format(name=name, title=name.capitalize())
                )
            found = True
            break
        if not found:
            raise Exception('invalid permission.py format')
        with open(os.path.join(path, 'permission.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)

    def append_lock(self, name, path):
        self.stdout.write(self.style.SUCCESS('append lock.py...'))

        lines = []
        with open(os.path.join(path, 'lock.py'), encoding='utf-8') as f:
            lines = f.readlines()
        if not lines:
            lines.append(
"""# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.lock import LOCKS as LIBLOCKS

LOCKS = LIBLOCKS + [
]
""")

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == ']':
                lines.insert(idx,
"""
    (
        '{name}',
        'gpermission.{name}_write',
        _('{title}'),
    ),
""".format(name=name, title=name.capitalize())
                )
            found = True
            break
        if not found:
            raise Exception('invalid lock.py format')
        with open(os.path.join(path, 'lock.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)

    def append_crud(self, name, path):
        self.stdout.write(self.style.SUCCESS('append crud.py...'))

        lines = []
        with open(os.path.join(path, 'crud.py'), encoding='utf-8') as f:
            lines = f.readlines()
        if not lines:
            lines.append(
"""# coding:utf-8

from library.config.crud import CRUD as LIBCRUD

CRUD = {
    **LIBCRUD,
}
""")

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == '}':
                lines.insert(idx,
"""
    '%(name)s': {
        'model': 'app.%(name)s.models.%(title)s',
        'permission': 'gpermission.%(name)s_write',
        'lock': '%(name)s',
    },
""" % {'name': name, 'title': name.capitalize()}
                )
            found = True
            break
        if not found:
            raise Exception('invalid crud.py format')
        with open(os.path.join(path, 'crud.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)

    def append_metadata(self, name, path):
        self.stdout.write(self.style.SUCCESS('append metadata.py...'))

        lines = []
        with open(os.path.join(path, 'metadata.py'), encoding='utf-8') as f:
            lines = f.readlines()
        if not lines:
            lines.append(
"""# coding:utf-8

from django.utils.translation import gettext_lazy as _
from library.config.metadata import METADATA as LIBMETADATA


METADATA = {
    **LIBMETADATA,
}
""")

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == '}':
                lines.insert(idx,
"""
    '%(name)s': {
        'models': [
            'app.%(name)s.models.%(title)s',
        ],
        'permission': 'gpermission.%(name)s_write',
        'lock': '%(name)s',
        'title': _('%(title)s'),
        'process': 'app.%(name)s.models.metadata_process',
    },
""" % {'name': name, 'title': name.capitalize()}
                )
            found = True
            break
        if not found:
            raise Exception('invalid metadata.py format')
        with open(os.path.join(path, 'metadata.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)

    def append_url(self, name, path):
        self.stdout.write(self.style.SUCCESS('append url.py...'))

        lines = []
        with open(os.path.join(path, 'urls.py'), encoding='utf-8') as f:
            lines = f.readlines()

        # urls.py will not empty, 'cause it is generated by django
        inited = False
        for line in lines:
            if 'library_urlpatterns' in line:
                inited = True
                break
        if not inited:
            lines.append('from library.config.urls import urlpatterns as library_urlpatterns\n')
            lines.append('\n')
            lines.append('\n')
            lines.append('urlpatterns = library_urlpatterns + [')
            lines.append(']')

        found = False
        for i in range(len(lines)):
            line = lines[i]
            if line.strip().startswith('urlpatterns'):
                lines.insert(i-2,
"""from app.{name} import views as {name}_views
""".format(name=name)
                             )
                found = True
                break
        if not found:
            raise Exception('invalid urls.py format')

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == ']':
                lines.insert(idx,
"""
    # {name}
    path('{name}/index',
         {name}_views.index,
         name='{name}_index'),
    path('{name}/addone',
         {name}_views.addone,
         name='{name}_addone'),
""".format(name=name)
                )
            found = True
            break
        if not found:
            raise Exception('invalid urls.py format')
        with open(os.path.join(path, 'urls.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)

    def append_module(self, name, path):
        self.stdout.write(self.style.SUCCESS('append module.py...'))

        lines = []
        with open(os.path.join(path, 'module.py'), encoding='utf-8') as f:
            lines = f.readlines()
        if not lines:
            lines.append(
"""from django.utils.translation import gettext_lazy as _
from library.config.module import (MODULES as LIBMODULES, merge_modules)


MODULES = merge_modules([
], LIBMODULES)
""")

        found = False
        for i in range(len(lines)):
            idx = len(lines)-1-i
            line = lines[idx]
            if line.strip() == '], LIBMODULES)':
                lines.insert(idx,
"""
    {
        'name': '%(name)s',
        'title': _('%(title)s'),
        'icon': 'fas fa-briefcase',
        'module': [
            {
                'name': '%(name)s',
                'title': _('%(title)s'),
                'icon': 'fas fa-book',
                'url': '%(name)s_index',
                'permission': 'gpermission.%(name)s_read',
                'lock': '%(name)s',
                'metadata': '%(name)s',
            },
        ],
    },
""" % {'name': name, 'title': name.capitalize()}
                )
            found = True
            break
        if not found:
            raise Exception('invalid module.py format')
        with open(os.path.join(path, 'module.py'), 'w', encoding='utf-8') as f:
            f.writelines(lines)
