# coding:utf-8

import json
from django.core import serializers
from django.db import models

from django.conf import settings
from pypuzlib import httprpc as rpc





class Whitelistip(models.Model):
    name = models.CharField(max_length=128, unique=True)


def get_model(id):
    try:
        return Whitelistip.objects.get(id=id)
    except Exception:
        return {}


def add_model(name):
    m = Whitelistip(name=name)
    m.save()


def metadata_process():
    return json.dumps([p.get('fields', {}) for p in json.loads(
        serializers.serialize('json',
                              Whitelistip.objects.all()))])

# get whitelist by ip
def get_whitelist_by_ip():
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('getWhitelistByIp', [])
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}


def mod_whitelist_ip(ip, op):
    req = {'ip_addr': ip, 'append_or_remove': op}
    c, m, p = rpc.clients[settings.GMT_APP_NAME].request('sendWhitelistModIp', req)
    if c != 0:
        raise Exception('error: code:{}, message:{}'.format(c, m))
    return p if p else {}

