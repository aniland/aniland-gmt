# coding:utf-8

from django.contrib.auth.decorators import (
    login_required, permission_required
)
from django.shortcuts import redirect
from library import layout
from library.lock import lock_required
from app.GmtTest import models


@login_required
@permission_required('gpermission.GmtTest_read')
def index(request):
    m = models.get_model(1)
    ctx = {
        'GmtTest': {
            'name': m.name if m else 'GmtTest',
        },
    }
    return layout.render_with_layout(request,
                                     'GmtTest',
                                     'GmtTest_index.html',
                                     ctx)


@login_required
@permission_required('gpermission.GmtTest_write')
@lock_required('GmtTest')
def addone(request):
    models.add_model(request.POST['name'])
    return redirect(request.POST['redirect'])
