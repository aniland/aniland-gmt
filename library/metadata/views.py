# coding: utf-8

import tarfile
import uuid
import shutil
import os
import json
import logging
from functools import wraps
from django.http import (HttpResponseBadRequest,
                         HttpResponseServerError,
                         FileResponse)
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.translation import gettext as _
from django.contrib import messages
from library import layout
from library.lock import lock_required
from library import lock as LockLib
from library.layout import response_json
from library.metadata import models


logger = logging.getLogger('library.metadata')


def metadata_management_satisfied(request):
    for name in list(models.METADATA.keys()):
        md = models.METADATA.get(name, {})
        if not md:
            return False, 'unknown metadata:{}'.format(name)

        if not request.user.has_perm(md['permission']):
            return False, 'no permission of metadata:{}'.format(name)

        if not LockLib.has_lock(request.user.username, md['lock']):
            return False, 'have not get lock of metadata:{}'.format(name)

    return True, ""


def metadata_protector(func):
    def metadata_name(request):
        if request.method == 'GET':
            name = request.GET.get('name', '')
        elif request.method == 'POST':
            if request.is_ajax():
                body = json.loads(request.body)
                name = body.get('name', '')
            else:
                name = request.POST.get('name', '')
        else:
            name = ''
        return name

    def error_handler(request, message, erroresponse):
        if request.is_ajax():
            return response_json(code=-1, message=message)
        else:
            messages.error(request, message)
            return erroresponse

    @wraps(func)
    def wrapper(request, *args, **kwargs):
        name = metadata_name(request)
        if not name:
            return error_handler(request,
                                 _("missing name param"),
                                 HttpResponseBadRequest())

        mdconf = models.METADATA.get(name, '')
        if not mdconf:
            return error_handler(request,
                                 _("unknown metadata:{}").format(name),
                                 HttpResponseBadRequest())

        perm = mdconf['permission']
        if not request.user.has_perm(perm):
            return error_handler(request,
                                 _("no permission"),
                                 HttpResponseBadRequest())

        lockname = mdconf['lock']
        if not LockLib.has_lock(request.user.username, lockname):
            return error_handler(request,
                                 _("not locked"),
                                 HttpResponseBadRequest())

        try:
            return func(request, *args, **kwargs)
        except Exception as ex:
            logger.exception(ex)
            return error_handler(request,
                                 str(ex),
                                 HttpResponseServerError())
    return wrapper


@login_required
@permission_required('gpermission.metadata_management_read')
def metadata_management_index(request):
    names = list(models.METADATA.keys())
    lives = models.get_live_metadata_infos(names)
    currents = models.get_current_metadata_infos(names)

    mds = []
    for name in names:
        md = models.METADATA[name]
        mds.append({
            'name': name,
            'lockname': md['lock'],
            'live': lives.get(name, {'name': name}),
            'current': currents.get(name, {'name': name}),
            'locked_by': LockLib.lock_owner(md['lock']),
            'has_perm': request.user.has_perm(md['permission']),
        })
    return layout.render_with_layout(request,
                                     'metadata_management',
                                     'metadata_management_index.html',
                                     {
                                         'metadata_management': {
                                             'metadatas': mds,
                                             'names': names,
                                         },
                                     })


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_lock_all(request):
    for name in list(models.METADATA.keys()):
        md = models.METADATA.get(name, {})
        if not md:
            continue

        if not request.user.has_perm(md['permission']):
            continue

        ok, _ = LockLib.lock(md['lock'], request.user)
        if not ok:
            continue

    return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_unlock_all(request):
    for name in list(models.METADATA.keys()):
        md = models.METADATA.get(name, {})
        if not md:
            continue

        if not request.user.has_perm(md['permission']):
            continue

        ok, _ = LockLib.unlock(md['lock'], request.user)
        if not ok:
            continue

    return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_import_backup(request):
    ok, msg = metadata_management_satisfied(request)
    if not ok:
        messages.error(request, msg)
        return redirect(request.POST['redirect'])

    tardir = ''
    tarname = ''
    try:
        uploadfile = request.FILES.get('filename', None)
        if not uploadfile:
            messages.error(request, 'on file uploaded')
            return redirect(request.POST['redirect'])

        uid = uuid.uuid1().hex

        tarname = os.path.join(models.TMP_DIR, uid + '.tar.gz')
        if os.path.exists(tarname):
            os.remove(tarname)
        if not os.path.exists(os.path.dirname(tarname)):
            os.makedirs(os.path.dirname(tarname))

        tardir = os.path.join(models.TMP_DIR, uid)
        if os.path.exists(tardir):
            shutil.rmtree(tardir)
        os.makedirs(tardir)

        with open(tarname, 'wb') as f:
            f.write(uploadfile.read())

        tar = tarfile.open(tarname, "r:gz")
        file_names = tar.getnames()
        for file_name in file_names:
            tar.extract(file_name, tardir)
        tar.close()

        if not os.path.exists(os.path.join(tardir, 'index.meta')):
            raise Exception('index.meta is missing')

        with open(os.path.join(tardir, 'index.meta'), encoding='utf-8') as f:
            meta = json.loads(f.read())

        if not meta:
            raise Exception('index.meta is empty')

        names = list(models.METADATA.keys())
        for k, v in meta.items():
            if k not in names:
                continue

            filename = v['file']
            with open(os.path.join(tardir, filename), encoding='utf-8') as ff:
                models.clear_metadata_source(k)
                models.restore_metadata_from_source(k, ff.read())
                models.save_metadata(k, v['author'], v['comment'])
    except Exception as ex:
        logger.exception(ex)
        messages.error(request, str(ex))
    finally:
        if os.path.exists(tardir):
            shutil.rmtree(tardir, ignore_errors=True)
        if os.path.exists(tarname):
            os.remove(tarname)
        return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_export_backup(request):
    ok, msg = metadata_management_satisfied(request)
    if not ok:
        messages.error(request, msg)
        return redirect(request.GET['redirect'])

    names = list(models.METADATA.keys())
    lives = models.get_live_metadata_infos(names)

    tmpfolder = ''
    tarname = ''
    try:
        tmpfolder = os.path.join(models.TMP_DIR, uuid.uuid1().hex)
        if os.path.exists(tmpfolder):
            shutil.rmtree(tmpfolder)
        os.makedirs(tmpfolder)

        meta = {}
        for live in lives.values():
            if not live['hash'] or not live['name']:
                continue
            md = models.Metadata.objects.get(name=live['name'],
                                             hash=live['hash'])
            source = models.get_metadata_source(md.id)

            filename = os.path.join(tmpfolder, live['name']+'.json')
            with open(filename, 'w', encoding='utf-8') as f:
                f.write(source.content)

                meta[live['name']] = {
                    'name': live['name'],
                    'hash': live['hash'],
                    'author': md.author,
                    'time': str(md.time),
                    'comment': md.comment,
                    'size': md.size,
                    'file': os.path.basename(filename),
                }

        with open(os.path.join(tmpfolder, 'index.meta'),
                  'w',
                  encoding='utf-8') as f:
            f.write(json.dumps(meta))

        tarname = os.path.join(models.TMP_DIR, 'metadata-backup.tar.gz')
        if os.path.exists(tarname):
            os.remove(tarname)
        if not os.path.exists(os.path.dirname(tarname)):
            os.makedirs(os.path.dirname(tarname))

        tar = tarfile.open(tarname, 'w:gz')
        for root, dir, files in os.walk(tmpfolder):
            for file in files:
                if file.endswith('.json') or file.endswith('.meta'):
                    fullpath = os.path.join(root, file)
                    tar.add(fullpath, arcname=file)
        tar.close()

        with open(tarname, 'rb') as file:
            # response = FileResponse(file)
            # response['Content-Type'] = 'application/octet-stream'
            # disposition = 'attachment;filename="{}"'.format(
            #     'metadata-backup.tar.gz')
            # response['Content-Disposition'] = disposition
            # return response
            return layout.response_bin_file(file.read(),
                                            'metadata-backup.tar.gz')
    except Exception as ex:
        logger.exception(ex)
        messages.error(request, str(ex))
        return redirect(request.GET['redirect'])
    finally:
        if os.path.exists(tmpfolder):
            shutil.rmtree(tmpfolder, ignore_errors=True)
        if os.path.exists(tarname):
            os.remove(tarname)


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_snapshot(request):
    ok, msg = metadata_management_satisfied(request)
    if not ok:
        messages.error(request, msg)
        return redirect(request.GET['redirect'])

    names = list(models.METADATA.keys())
    lives = models.get_live_metadata_infos(names)

    tmpfolder = ''
    tarname = ''
    try:
        tmpfolder = os.path.join(models.TMP_DIR, uuid.uuid1().hex)
        if os.path.exists(tmpfolder):
            shutil.rmtree(tmpfolder)
        os.makedirs(tmpfolder)

        meta = {}
        for live in lives.values():
            if not live['hash'] or not live['name']:
                continue
            md = models.Metadata.objects.get(name=live['name'],
                                             hash=live['hash'])
            processed = models.get_metadata_processed(md.id)

            filename = os.path.join(tmpfolder, live['name']+'.json')
            with open(filename, 'w', encoding='utf-8') as f:
                f.write(processed.content)

                meta[live['name']] = {
                    'name': live['name'],
                    'hash': live['hash'],
                    'file': os.path.basename(filename),
                }

        with open(os.path.join(tmpfolder, 'index.meta'),
                  'w',
                  encoding='utf-8') as f:
            f.write(json.dumps(meta))

        tarname = os.path.join(models.TMP_DIR, 'metadata-snapshot.tar.gz')
        if os.path.exists(tarname):
            os.remove(tarname)
        if not os.path.exists(os.path.dirname(tarname)):
            os.makedirs(os.path.dirname(tarname))

        tar = tarfile.open(tarname, 'w:gz')
        for root, dir, files in os.walk(tmpfolder):
            for file in files:
                if file.endswith('.json') or file.endswith('.meta'):
                    fullpath = os.path.join(root, file)
                    tar.add(fullpath, arcname=file)
        tar.close()

        with open(tarname, 'rb') as file:
            # response = FileResponse(file)
            # response['Content-Type'] = 'application/octet-stream'
            # disposition = 'attachment;filename="{}"'.format(
            #     'metadata-snapshot.tar.gz')
            # response['Content-Disposition'] = disposition
            # return response
            return layout.response_bin_file(file.read(),
                                            'metadata-snapshot.tar.gz')
    except Exception as ex:
        logger.exception(ex)
        messages.error(request, str(ex))
        return redirect(request.GET['redirect'])
    finally:
        if os.path.exists(tmpfolder):
            shutil.rmtree(tmpfolder, ignore_errors=True)
        if os.path.exists(tarname):
            os.remove(tarname)


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_restore_all(request):
    names = list(models.METADATA.keys())
    lives = models.get_live_metadata_infos(names)
    currents = models.get_current_metadata_infos(names)

    for name in names:
        live_hash = lives.get(name, {}).get('hash', '')
        if not live_hash:
            continue
        current_hash = currents.get(name, {}).get('hash', '')
        if live_hash == current_hash:
            continue

        md = models.Metadata.objects.get(name=name, hash=live_hash)
        source = models.get_metadata_source(md.id)
        models.clear_metadata_source(name)
        models.restore_metadata_from_source(name, source.content)
    return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_pushlive_all(request):
    names = list(models.METADATA.keys())
    lives = models.get_live_metadata_infos(names)
    currents = models.get_current_metadata_infos(names)

    for name in names:
        current_hash = currents.get(name, {}).get('hash', '')
        if not current_hash:
            continue
        live_hash = lives.get(name, {}).get('hash', '')
        if live_hash == current_hash:
            continue

        md = models.Metadata.objects.get(name=name, hash=current_hash)
        models.pushlive_metadata(name, md.id)
    return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_restore(request):
    name = request.POST['name']
    lives = models.get_live_metadata_infos([name])
    currents = models.get_current_metadata_infos([name])

    live_hash = lives.get(name, {}).get('hash', '')
    if not live_hash:
        return redirect(request.POST['redirect'])

    current_hash = currents.get(name, {}).get('hash', '')
    if live_hash == current_hash:
        return redirect(request.POST['redirect'])

    md = models.Metadata.objects.get(name=name, hash=live_hash)
    source = models.get_metadata_source(md.id)
    models.clear_metadata_source(name)
    models.restore_metadata_from_source(name, source.content)
    return redirect(request.POST['redirect'])


@login_required
@permission_required('gpermission.metadata_management_write')
@lock_required('metadata_management')
def metadata_management_pushlive(request):
    name = request.POST['name']
    lives = models.get_live_metadata_infos([name])
    currents = models.get_current_metadata_infos([name])

    current_hash = currents.get(name, {}).get('hash', '')
    if not current_hash:
        return redirect(request.POST['redirect'])

    live_hash = lives.get(name, {}).get('hash', '')
    if live_hash == current_hash:
        return redirect(request.POST['redirect'])

    md = models.Metadata.objects.get(name=name, hash=current_hash)
    models.pushlive_metadata(name, md.id)
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_refresh(request):
    m = request.GET['name']
    processed = models.generate_metadata_processed(m)
    current_hash = models.generate_metadata_hash(processed)

    live = models.get_live_metadata(m)
    return response_json({
        'live_hash': live['hash'] if live else '',
        'current_hash': current_hash,
    })


@login_required
@metadata_protector
def metadata_clear(request):
    models.clear_metadata_source(request.POST['name'])
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_save(request):
    id = models.save_metadata(request.POST['name'],
                              request.user.username,
                              request.POST['comment'])
    if id and request.POST.get('pushlive', False):
        models.pushlive_metadata(request.POST['name'], id)
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_delete(request):
    models.delete_metadata(request.POST['id'])
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_pushlive(request):
    models.pushlive_metadata(request.POST['name'],
                             request.POST['id'])
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_restore(request):
    source = models.get_metadata_source(request.POST['id'])
    models.clear_metadata_source(request.POST['name'])
    models.restore_metadata_from_source(request.POST['name'],
                                        source.content)
    return redirect(request.POST['redirect'])


@login_required
@metadata_protector
def metadata_export(request):
    source = models.get_metadata_source(request.GET['id'])
    return layout.response_json_file(source.content,
                                     'metadata-export-{}.json'.format(
                                         request.GET['name']))


@login_required
@metadata_protector
def metadata_view(request):
    processed = models.get_metadata_processed(request.GET['id'])
    formatted = json.dumps(json.loads(processed.content),
                           indent=4,
                           separators=(', ', ': '))
    return layout.response_json_file(formatted,
                                     'metadata-view-{}.json'.format(
                                         request.GET['name']))


@login_required
@metadata_protector
def metadata_import(request):
    uploadfile = request.FILES.get('filename', None)
    if not uploadfile:
        return redirect(request.POST['redirect'])

    models.clear_metadata_source(request.POST['name'])
    models.restore_metadata_from_source(request.POST['name'],
                                        uploadfile.read())
    return redirect(request.POST['redirect'])
